"use strict";

/**
 * The Thermostat Arc view mixin holds all of the common view logic for the temperature slider arc, thermostat mode menu,
 * frost point modal and hold modal.
 * Any view that's needs to show the LARGE version of the arc with these modals and menus should use this mixin.
 * intended to be mixed-in to the marionette view object and has a few basic responsibilities:
 * - hold similar thermostat arc level view code
 * - manage related menus and modals
 * - manage setting and getting and displaying Ayla properties for the thermostat
 */
define([
	"app",
	"bluebird",
	"common/config",
	"common/constants",
	"consumer/views/SalusModal.view",
	"consumer/dashboard/views/tileContentViews/thermostatSpecific/ModeMenu.view",
	"consumer/equipment/views/HoldTempPointModal.view"
], function (App, P, config, constants, SalusModalView, ModeMenuModal, HoldTempPointModal) {

	App.module("Consumer.Views.Mixins", function (Mixins, App, B, Mn, $, _) {
		Mixins.ThermostatArcView = function (/*options*/) {
			this.addToObj({
				events: {
					"click .bb-main-mode-icon": "toggleModeMenu",
					"click .bb-fan-mode-icon": "toggleFanMenu",
					"click .bb-hold-mode-icon": "toggleHoldMenu"
				},
				ui: {
					tstatArc: ".bb-tstat-arc",
					tstatDestinationTemp: ".bb-arc-set-point-temp",
					tstatCurrentTemp: ".bb-arc-current-temp",
					fanModeIcon: ".bb-fan-mode-icon",
					holdModeIcon: ".bb-hold-mode-icon",
					mainModeIcon: ".bb-main-mode-icon"
				}
			});

			// This needs to run after initialize because we can't overwrite the "initialize" method in setDefaults.
			this.after('initialize', function () {
				var that = this;

				// so many binds, but better safe than sorry
				_.bindAll(this,
					"handleDomLoadComplete",
					"toggleModeMenu",
					"toggleFanMenu",
					"toggleHoldMenu",
					"openModeMenu",
					"openFanMenu",
					"openHoldMenu",
					"closeModeMenu",
					"closeFanMenu",
					"closeHoldMenu",
					"initiateSetpointPropRefreshPoll",
					"clearSetpointTimeout",
					"refreshSetpointProps",
					"_handleTempSliderDragContinuous",
					"_handleTempSliderDragEnd",
					"_getWhichSetPointToRead",
					"_showSetHoldPointModal",
					"_setTemperatureWithPollTimeOut"
				);

				this.setTemperatureIdlePollTimeout = null;
				this.devicePropsPollingInterval = null;
				this.model.allowChange = true;

				this.listenTo(this.model, "change:connection_status", function () {
					that.ui.tstatArc.roundSlider(that.model.isOnline() && !that.model.isLeaveNetwork() ? "enable": "disable");
				});
				
				// TODO update this for the non-flip case of equipment widget view
				this.listenTo(this, "tile:flipToFront", function () {
					// Kill off our poll interval so the header can pickup polling at 1 minute again
					//that.clearSetpointTimeout();
					that.closeModeMenu();
					that.closeFanMenu();
					that.closeHoldMenu();
				});

				// TODO update this for the non-flip case of equipment widget view
				this.listenTo(this, "tile:flipToBack", function () {
					// Poll the model refresh so any changes from the hardware device can showup in near-realtime on our UI!
					// 20 seconds was chosen because it seems to be how long it takes for the hardware to update a value in Ayla
					//that.initiateSetpointPropRefreshPoll();
					if (!that.backTstatArcOK) {
						that.handleDomLoadComplete();
					}
				});
			});

			this.before("destroy", function () {
				//this.clearSetpointTimeout();
				if (!!this.ui.tstatArc  && this.ui.tstatArc.roundSlider) {
					this.ui.tstatArc.roundSlider("destroy");
				}
				
				this.ui.tstatArc.off();
				if (this.modeMenuView) {
					this.modeMenuView.destroy();
				}

				if (this.fanModeMenuView) {
					this.fanModeMenuView.destroy();
				}

				if (this.holdMenuView) {
					this.holdMenuView.destroy();
				}
			});

			/**
			 * detach appropriate button
			 * setup bindings for common models (arc set point, systemMode icon)
			 * setup bindings for optima
			 * setup bindings for it600
			 */
			this.after("render", function () {
				var that = this;
				
				if (!this.model.isIT600) { // if optima
					this.ui.holdModeIcon.detach();
				} else {
					this.ui.fanModeIcon.detach();
					this.ui.mainModeIcon.addClass("default-cursor");
				}

				this.addBinding(null, ".bb-arc-set-point-temp", {
					observe: "temporaryTemperatureValue", // This is the value we set temporarily as the user slides the arc slider before sending it off to the cooling or heating setpoint endpoint
					onGet: function (value /*, options*/) {
						var backRoundSliderInstance;

						value = value || config.thermostatMinimumTemp;

						if (!!this.ui.tstatArc) {
							backRoundSliderInstance = this.ui.tstatArc.data("roundSlider");
						}

						if (backRoundSliderInstance) {
							backRoundSliderInstance.setValue(value);
						}
						
						//99 is a special value, must be fix to MaxCoolSetpoint_x100.
						if (value === 99) {
							value = that.model.getPropertyValue("MaxCoolSetpoint_x100") / 100;
						}

						return value;
					}
				});

				this.addBinding(null, ".bb-arc-current-temp", {
					observe: "LocalTemperature_x100",
					onGet: function (value) {
						if (value) {
							return value.getProperty("value") ? value.getProperty("value") / 100 : config.thermostatMinimumTemp;
						}

						return config.thermostatMinimumTemp;
					}
				});

				this.addBinding(null, ".bb-main-mode-icon", {
					observe: ["SystemMode", "RunningMode"],
					onGet: function (props) {
						return [props[0] ? props[0].getProperty() : null, props[1] ? props[1].getProperty() : null];
					},
					update: function ($el, vals) {
						var modeClassName,
							classNameArray = [
								"power-icon",
								"auto-icon",
								"cooling-icon-on",
								"heating-icon-on",
								"cooling-icon-off",
								"heating-icon-off"
							];

						$el.removeClass(classNameArray.join(" "));

						var systemMode = vals[0],
							runningMode = vals[1];
						
						if (_.isNull(systemMode) || _.isNull(runningMode)) {
							modeClassName = classNameArray[0];
						} else {
							switch (systemMode) {
								case constants.thermostatModeTypes.OFF:
									modeClassName = classNameArray[0];
									break;
								case constants.thermostatModeTypes.AUTO:
									modeClassName = classNameArray[1];
									break;
								case constants.thermostatModeTypes.COOL:
									if (runningMode === constants.runningModeTypes.OFF) {
										modeClassName = classNameArray[4];
									} else if (runningMode === constants.runningModeTypes.COOL) {
										modeClassName = classNameArray[2];
									}
									break;
								case constants.thermostatModeTypes.HEAT:
								case constants.thermostatModeTypes.EMERGENCYHEATING:
									if (runningMode === constants.runningModeTypes.OFF) {
										modeClassName = classNameArray[5];
									} else if (runningMode === constants.runningModeTypes.HEAT) {
										modeClassName = classNameArray[3];
									}
									break;
								default:
									modeClassName = classNameArray[0];
									break;
							}
						}

						$el.addClass(modeClassName);
						
						if (modeClassName === "power-icon") {
							that.ui.tstatArc.roundSlider("disable");
						} else {
							that.ui.tstatArc.roundSlider("enable");
						}
					}
				});

				if (!this.model.isIT600) {
					// optima specific - fan mode
					this.addBinding(null, ".bb-fan-mode-icon", {
						observe: "FanMode",
						onGet: function (prop) {
							return prop ? prop.getProperty() : null;
						},
						update: function ($el, val) {
							var modeClassName,
								classNameArray = [
									"fan-auto",
									"fan-on"
								];

							$el.removeClass(classNameArray.join(" "));

							if (_.isNull(val)) {
								modeClassName = classNameArray[0];
							} else {
								switch (val) {
									case constants.fanModeTypes.AUTO:
										modeClassName = classNameArray[0];
										break;
									case constants.fanModeTypes.ON:
										modeClassName = classNameArray[1];
										break;
									default:
										modeClassName = classNameArray[0];
										break;
								}
							}

							$el.addClass(modeClassName);
						}
					});
				} else {
					// it600 specific - hold icon
					this.addBinding(null, ".bb-hold-mode-icon", {
						observe: "HoldType",
						onGet: function (prop) {
							return prop ? prop.getProperty() : null;
						},
						update: function ($el, val) {
							var modeClassName,
								classNameArray = [
									"hold-auto-icon", // follow schedule
									"hold-temporary-icon",
									"hold-permanent-icon",
									"power-icon"
								];

							$el.removeClass(classNameArray.join(" "));

							if (_.isNull(val)) {
								modeClassName = classNameArray[0];
							} else {
								switch (val) {
									case constants.it600HoldTypes.FOLLOW:
										modeClassName = classNameArray[0];
										break;
									case constants.it600HoldTypes.TEMPHOLD:
										modeClassName = classNameArray[1];
										break;
									case constants.it600HoldTypes.PERMHOLD:
										modeClassName = classNameArray[2];
										break;
									case constants.it600HoldTypes.OFF:
										modeClassName = classNameArray[3];
										break;
									default:
										modeClassName = classNameArray[0];
										break;
								}
							}

							$el.addClass(modeClassName);
							
							if (val === constants.it600HoldTypes.OFF) {
								that.ui.tstatArc.roundSlider("disable");
							} else {
								that.ui.tstatArc.roundSlider("enable");
							}
						}
					});
				}
			});

			this.setDefaults({
				initiateSetpointPropRefreshPoll: function () {
					this.devicePropsPollingInterval = setInterval(this.refreshSetpointProps, config.devicePollingInformation.devicePropsPollingInterval);
				},
				clearSetpointTimeout: function () {
					clearInterval(this.devicePropsPollingInterval);
				},
				refreshSetpointProps: function () {
					this.model.getDeviceProperties(true);
				},
				/**
				 * gets called when the DOM load is complete
				 */
				handleDomLoadComplete: function () {
					var that = this,
						arcLocalTempToDisplay,
						tstatOuterWidth;

					this.model.getDevicePropertiesPromise().then(function () {
						if (!that.isDestroyed) {
							var whichSetPoint = that._getWhichSetPointToRead();

							// temporaryTemperatureValue is local only, we don't need to getProperty() it
							if (whichSetPoint.indexOf("temporary") > -1) {
								arcLocalTempToDisplay = that.model.get(whichSetPoint);
							} else if (that.model.get(whichSetPoint)) {
								arcLocalTempToDisplay = that.model.getPropertyValue(that._getWhichSetPointToRead()) / 100;
								that.model.set("temporaryTemperatureValue", arcLocalTempToDisplay || config.thermostatMinimumTemp);
							}

							/*
							 set readonly if it600 is in off
							 TODO: this breaks arc-output
							 readOnly = that.model.isIT600 && (that.model.get("HoldType") && that.model.getPropertyValue("HoldType") === constants.it600HoldTypes.OFF);
							 */

							if (!!that.ui.tstatArc) {
								tstatOuterWidth = that.ui.tstatArc.innerWidth();

								that.ui.tstatArc.roundSlider({
									sliderType: "min-range",
									handleShape: "round",
									circleShape: "pie",
									handleSize: "+20",
									radius: tstatOuterWidth || 1,
									min: that.model.getPropertyValue("MinCoolSetpoint_x100") / 100 || config.thermostatMinimumTemp,
									max: that.model.getPropertyValue("MaxHeatSetpoint_x100") / 100 || config.thermostatMaximumTemp,
									step: 0.5,
									value: arcLocalTempToDisplay || config.thermostatMinimumTemp,
									startAngle: 315,
									width: 10, // Value came from measuring the thermostat arc specs
									showTooltip: false,
									animation: true
								});
								
								that.backTstatArcOK = true;
								
								if (!that.model.isIT600) {
									if (!that.model.isOnline() || that.model.isLeaveNetwork() || that.model.getPropertyValue("SystemMode") === constants.thermostatModeTypes.OFF) {
										that.ui.tstatArc.roundSlider("disable");
									} else {
										that.ui.tstatArc.roundSlider("enable");
									}
								} else {
									if (!that.model.isOnline() || that.model.isLeaveNetwork() || that.model.getPropertyValue("HoldType") === constants.it600HoldTypes.OFF) {
										that.ui.tstatArc.roundSlider("disable");
									} else {
										that.ui.tstatArc.roundSlider("enable");
									}
								}

								that.ui.tstatArc.on("start", function (event) {
									that._dragging = true;
									that._handleTempSliderDragContinuous(event);
								});

								// Update the set point value during a slider drag or change (change occurs when tapping anywhere on the arc)
								that.ui.tstatArc.on("drag change", function (event) {
									that._handleTempSliderDragContinuous(event);

									// if we've clicked the dial act like we dragged it.
									if (event.type === "change" && !that.dragging) {
										that._handleTempSliderDragEnd();
									}
								});

								// Remove the animation class applied to the set point font to make it grow
								that.ui.tstatArc.on("stop", function () {
									that._dragging = false;
									that._handleTempSliderDragEnd();
								});
							}

							that.modeMenuView = new ModeMenuModal.MenuContainer({
								model: that.model
							});

							that.listenTo(that.modeMenuView, "close:menu", that.closeModeMenu);
							that.$el.append(that.modeMenuView.render().$el);
							that.closeModeMenu();

							if (!that.model.isIT600) { // if optima
								that.fanModeMenuView = new ModeMenuModal.MenuContainer({
									model: that.model,
									fanMenu: true
								});

								that.listenTo(that.fanModeMenuView, "close:menu", that.closeFanMenu);
								that.$el.append(that.fanModeMenuView.render().$el);
								that.closeFanMenu();
							} else {
								that.holdMenuView = new ModeMenuModal.MenuContainer({
									model: that.model,
									holdMenu: true
								});

								that.listenTo(that.holdMenuView, "close:menu", that.closeHoldMenu);
								that.$el.append(that.holdMenuView.render().$el);
								that.closeHoldMenu();
							}
						}

					});
				},

				toggleModeMenu: function () {
					if (!this.noMixinEvents && !this.model.isIT600 && this.model.isOnline() && !this.model.isLeaveNetwork()) {
						//close others
						this.closeFanMenu();
						this.closeHoldMenu();

						if (this.modeMenuView.$el.is(":visible")) {
							this.closeModeMenu();
						} else {
							this.openModeMenu();
						}
					}
				},

				closeModeMenu: function () {
					if (this.modeMenuView) {
						this.modeMenuView.$el.toggle(false);
					}
				},

				openModeMenu: function () {
					if (this.modeMenuView) {
						this.modeMenuView.$el.toggle(true);
					}
				},

				toggleFanMenu: function () {
					if (!this.model.isIT600 && !this.noMixinEvents && this.model.isOnline() && !this.model.isLeaveNetwork()) { // if optima
						// close others
						this.closeModeMenu();
						this.closeHoldMenu();

						if (this.fanModeMenuView.$el.is(":visible")) {
							this.closeFanMenu();
						} else {
							this.openFanMenu();
						}
					}
				},

				closeFanMenu: function () {
					if (this.fanModeMenuView) {
						this.fanModeMenuView.$el.toggle(false);
					}
				},

				openFanMenu: function () {
					if (this.fanModeMenuView) {
						this.fanModeMenuView.$el.toggle(true);
					}
				},

				toggleHoldMenu: function () {
					if (this.model.isIT600 && !this.noMixinEvents && this.model.isOnline() && !this.model.isLeaveNetwork()) { // if it600
						this.closeModeMenu();
						this.closeFanMenu();

						if (this.holdMenuView.$el.is(":visible")) {
							this.closeHoldMenu();
						} else {
							this.openHoldMenu();
						}
					}
				},

				closeHoldMenu: function () {
					if (this.holdMenuView) {
						this.holdMenuView.$el.toggle(false);
					}
				},

				openHoldMenu: function () {
					if (this.holdMenuView) {
						this.holdMenuView.$el.toggle(true);
					}
				},

				/**
				 * This updates the set point value as the slider is dragged
				 * @param event
				 * @private
				 */
				_handleTempSliderDragContinuous: function (event) {
					// Add an animation class to the set point font so it grows when this starts updating
					this.ui.tstatDestinationTemp.addClass("updating");

					// close any menus
					this.closeFanMenu();
					this.closeHoldMenu();
					this.closeModeMenu();
					
					if (event.value) {
						this.model.set("temporaryTemperatureValue", event.value);
					}
				},
				_handleTempSliderDragEnd: function () {
					var $arcSpinnerElement = _.template("<div class='bb-arc-spinner spinner-dynamic-arc-interior'></div>")();

					this.ui.tstatDestinationTemp.removeClass("updating");
					// TODO wire this so it blocks the action of sending data to Ayla until the user submits or cancels this modal, like frost points modal
					// check if 'dont show this again' is checked
					// TODO Commenting out, tasked to remove later, SCS-1261
					//if (!this.model.get("hide_set_hold_point_modal")) {
					//	this._showSetHoldPointModal();
					//}

					if (this.ui.tstatArc.find(".bb-arc-spinner").length === 0) {
						this.ui.tstatArc.find("span.rs-block").prepend($arcSpinnerElement);
						//this.showSmallTileSpinner();
					}

					this._setTemperatureWithPollTimeOut();
				},
				_setTemperatureWithPollTimeOut: function () {
					var modelPropString = this._getWhichSetPointToRead(),
							modelProp = this.model.get(modelPropString);

					if (modelProp) {
						var that = this;
						// If the user keeps dragging the thermostat arc without waiting the three seconds, then keep resetting
						// the set thermostat timeout so we don't write the value to Ayla until the user is really done.
						// The Optima zigbee thermostat manual states the hardware waits 3 seconds after the user changes the
						// temperature before it writes it to Ayla. We are replicating this functionality.
						if (this.setTemperatureIdlePollTimeout) {
							clearTimeout(this.setTemperatureIdlePollTimeout);
							this.setTemperatureIdlePollTimeout = null;
						}

						that.model.allowChange = false;
						this.setTemperatureIdlePollTimeout = setTimeout(function () {
							// Once the property is propagated to the getter property, remove the loading spinner
							that.listenToOnce(modelProp, "propertiesSynced", function () {
								that.model.allowChange = true;
								//that.hideSmallTileSpinner();
							});

							that.listenToOnce(modelProp, "propertiesFailedSync", function () {
								that.model.allowChange = true;
								that.ui.tstatArc.find(".bb-arc-spinner").remove();
								App.Consumer.Controller.showDeviceSyncError(that.model);
							});

							if (modelProp) {
								modelProp.setProperty(that.model.get("temporaryTemperatureValue") * 100).then(function () {
									that.ui.tstatArc.find(".bb-arc-spinner").remove();
								});
							} else {
								// TODO
								App.warn("TODO: figure out which temp property to set");
							}
						}, config.thermostatSetPointDelay);
					}
				},
				/**
				 * Figure out which setpoint value to read based on the currently set mode, cooling or heating
				 * @returns {*}
				 * @private
				 */
				_getWhichSetPointToRead: function () {
					var temperatureEndpoint, systemMode = this.model.get("SystemMode");

					if (systemMode) {
						switch (systemMode.getProperty()) {
							case constants.thermostatModeTypes.EMERGENCYHEATING:
							case constants.thermostatModeTypes.HEAT:
								temperatureEndpoint = "HeatingSetpoint_x100";
								break;
							case constants.thermostatModeTypes.COOL:
								temperatureEndpoint = "CoolingSetpoint_x100";
								break;
							default:
								temperatureEndpoint = "temporaryTemperatureValue";
								break;
						}
					} else {
						temperatureEndpoint = "temporaryTemperatureValue";
					}

					return temperatureEndpoint;
				},
				_showSetHoldPointModal: function () {
					var holdTempPointModalView = new HoldTempPointModal({model: this.model}),
						salusModalView = new SalusModalView({
							contentView: holdTempPointModalView
						});

					this.listenToOnce(holdTempPointModalView, "modal:closed", function () {
						App.hideModal();
					});

					App.modalRegion.show(salusModalView);
					App.showModal();
				}
			});
		};
	});

	return App.Consumer.Views.Mixins.ThermostatArcView;
});