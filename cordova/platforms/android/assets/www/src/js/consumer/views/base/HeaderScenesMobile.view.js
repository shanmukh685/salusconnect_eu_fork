"use strict";

define([
	"app",
	"bluebird",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"common/util/VendorfyCssForJs"
], function (App, P, constants, consumerTemplates, SalusView, VendorfyCssForJs) {

	/**
	 * TODO: all things named 'Scenes' need to be changed to MyStatus
	 */

	App.module("Consumer.Views", function (Views, App, B, Mn, $, _) {
		Views.SceneItemMobileView = Mn.ItemView.extend({
			template: consumerTemplates["base/headerSceneItem"],
			className: "scene-item-header-mobile col-xs-4",
			attributes: {
				role: "button"
			},
			ui: {
				name: ".bb-scene-name",
				selected: ".bb-scene-selected",
				gradientWithIcon: ".bb-scene-background"
			},
			bindings: {
				".bb-scene-name": {
					observe: "name"
				},
				".bb-scene-selected": {
					observe: "selected",
					update: function ($el, selected) {
						if (App.hasCurrentGateway()) {
							$el.toggleClass("selected", selected).toggleClass("deselected", !selected);
						} else {
							$el.addClass("invalid").removeClass("selected").removeClass("deselected");
						}
					}
				}
			},
			events: {
				"click": "_setMyStatus"
			},
			initialize: function (/*options*/) {
				_.bindAll(this, "_setMyStatus");
				this.clicking = false;
			},
			onRender: function () {
				var iconPaths = constants.myStatusIconPaths,
						colors = constants.getStatusIconBackgroundColor(this.model.get("icon"));

				VendorfyCssForJs.formatBgGradientWithImage(
						this.ui.gradientWithIcon,
						"center center/44% 44%",
						"no-repeat",
						App.rootPath(iconPaths[this.model.get("icon")]),
						"145deg",
						colors.topColor,
						"0%",
						colors.botColor,
						"100%",
						"/images/icons/myStatus/icon_question_mark.svg"
				);
			},
			_setMyStatus: function () {
				var that = this, myStatusProp, gatewayNode = App.getCurrentGateway().getGatewayNode(),myStatusKey;
                
                if(that.model.get("selected")){
                    myStatusKey="NULL";
                }else{
                    myStatusKey=that.model.get("key");
                }

				if (!this.clicking && gatewayNode) {
					this.clicking = true;
					this.showSpinner({textKey: "none"});

					gatewayNode.getDevicePropertiesPromise().then(function () {
						if ((myStatusProp = gatewayNode.get("MyStatus"))) {
							that.listenToOnce(myStatusProp, "propertiesSynced", that.hideSpinner);
							that.listenToOnce(myStatusProp, "propertiesFailedSync", that.hideSpinner);

							App.salusConnector.setMyStatus(myStatusKey).finally(function () {
								that.clicking = false;
							});
						} else {
							that.clicking = false;
							that.hideSpinner();

						}
					}).catch(function (err) {
						that.clicking = false;
						that.hideSpinner();

						return P.reject(err);
					});
				}
			}
		}).mixin([SalusView]);

		Views.HeaderScenesMobileView = Mn.CompositeView.extend({
			className: "col-xs-12 mobile-menu-row mobile-scenes-row",
			template: consumerTemplates["base/mobileScenesRow"],
			childView: Views.SceneItemMobileView,
			childViewContainer: ".bb-scenes-container",
			ui: {
				"statusList": ".bb-scenes-container"
			},
			events: {
				"click .bb-myStatus-link": "_clickLink"
			},
			initialize: function () {
				_.bindAll(this, "_clickLink");

				this.collection = App.salusConnector.getRuleGroupCollection();
			},
			onRender: function () {
				if (!this.collection || this.collection.isEmpty()) {
					this.ui.statusList.addClass("hidden");
				}
			},
			_clickLink: function () {
				App.navigate("myStatus");
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Views.HeaderScenesMobileView;
});