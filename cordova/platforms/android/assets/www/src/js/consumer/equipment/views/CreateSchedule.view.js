"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view"
], function (App, constants, consumerTemplates, SalusView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.CreateScheduleView = Mn.ItemView.extend({
			className: "schedule-menu-row row",
			template: consumerTemplates["equipment/myEquipment/createSchedule"],
			ui: {
				button: ".bb-schedules-button",
				subtext: ".bb-schedules-subtext"
			},
			initialize: function (options) {
				_.bindAll(this, "navigateSchedulesPage");

				options = options || {};

				this.categoryType = options.categoryType;

				this.schedulesButtonView = new App.Consumer.Views.SalusButtonPrimaryView({
					classes: "btn-warning",
					buttonTextKey: "equipment.myEquipment.categories.schedules.scheduleButton",
					clickedDelegate: this.navigateSchedulesPage
				});
			},
			onRender: function () {
				var subtext = "equipment.myEquipment.categories.schedules." + this.categoryType;

				this.ui.subtext.text(App.translate(subtext));
				this.ui.button.replaceWith(this.schedulesButtonView.render().$el);
			},
			onDestroy: function () {
				this.schedulesButtonView.destroy();
			},
			navigateSchedulesPage: function () {
				// trim off the last char from category enum, schedules paths use singular nouns (thermostats -> thermostat)
				var path = "schedules/" + this.categoryType.substr(0, this.categoryType.length - 1);

				App.navigate(path);
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views.CreateScheduleView;
});