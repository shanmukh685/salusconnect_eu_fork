"use strict";

define([
	"app",
	"momentWrapper",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, moment, constants, consumerTemplates, SalusView) {
	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn,$, _) {

		Views.UndoActionSelectView = Mn.ItemView.extend({
			className: "undo-action",
			template: consumerTemplates["oneTouch/undoActionSelect"],
			ui: {
				"undoActionList" : ".bb-undo-action-list"
			},
			initialize: function () {
				var that = this;

				_.bindAll(this, "getRuleData");

				this.undoActions = new B.Collection();

				App.Consumer.OneTouch.ruleMakerManager.getActions().each(function(actionModel) {
					that.undoActions.add(new B.Model(actionModel.toJSON()));
				});

				this.actionsCriteriaView = new Views.OneTouchCriteriaCollectionView({
					collection: this.undoActions,
					type: constants.oneTouchMenuTypes.action,
					useDeleteHook: true,
					deleteHookKey: "undoActionMenu"
				});

				this.listenTo(this.actionsCriteriaView, "criteriaCollection:undoActionMenu", this._handleDelete);
			},
			onRender: function () {
				this.ui.undoActionList
						.append(this.actionsCriteriaView.render().$el);
			},
			onDestroy: function () {
				this.actionsCriteriaView.destroy();
			},
			_handleDelete: function (argsObj) {
				var criteriaView = argsObj.criteriaView;
				this.undoActions.remove(this.undoActions.models[criteriaView._index]);
				criteriaView.destroy();
			},
			getRuleData: function () {
				// get the current list of actions to do
				var pendingActions = App.Consumer.OneTouch.ruleMakerManager.getActions();

				// get the actions attributes
				var pendActionIdArray = _.map(pendingActions.models, function (action) {
					return action.get("ActID");
				});

				// filter the then do later actions based on if they are in the current list of actions to do
				var validUndoActions = _.map(_.filter(this.undoActions.models, function (action) {
					return _.contains(pendActionIdArray, action.get("ActID"));
				}), function(validUndoAction) {
					return validUndoAction.toJSON();
				});

				return {
					type: constants.thenDoLaterSubTypes.undo,
					data: validUndoActions
				};
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.OneTouch.Views.UndoActionSelectView;
});