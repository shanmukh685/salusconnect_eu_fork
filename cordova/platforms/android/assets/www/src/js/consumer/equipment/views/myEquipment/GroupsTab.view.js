"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.salusTabContent",
	"common/view/SpinnerPage.view",
	"consumer/equipment/views/myEquipment/AddNewTile.view",
	"consumer/equipment/views/myEquipment/GroupsTiles.view"
], function (App, templates, SalusView, SalusTabView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn) {
		Views.Groups = Mn.CompositeView.extend({
			template: templates["equipment/myEquipment/groupsComposite"],
			childView: App.Consumer.Equipment.Views.GroupView,
			childViewContainer: ".list-container",
			initialize: function () {
				var that = this;
				this.collection = App.salusConnector.getGroupCollection();
				// get group collection for gateway
				App.salusConnector.getFullGroupCollection().refresh().then(function () {
					that.collection.reset(App.salusConnector.getGroupCollection().toArray());
				});
			},
			onRender: function () {
				if (!this.addGroupTile) {
					this.addGroupTile = new App.Consumer.Equipment.Views.AddNewTileView({
						i18nTextKey: "equipment.myEquipment.groups.addNewGroup",
						size: "regular",
						classes: "group-tile",
						clickDestination: "equipment/newGroup"
					});
				}

				var listContainer = this.$childViewContainer;

				if (!listContainer) {
					//$childViewContainer will be null if collection is empty.
					listContainer = this.$(".list-container");
				}

				listContainer.prepend(this.addGroupTile.render().$el);
			},
			onDestroy: function () {
				this.addGroupTile.destroy();
			}
		}).mixin([SalusView]);

		Views.Devices = Mn.CompositeView.extend({
			template: templates["equipment/myEquipment/devicesComposite"],
			childView: App.Consumer.Equipment.Views.UngroupedDeviceView,
			childViewContainer: ".list-container",
			initialize: function () {
				this.collection = App.salusConnector.getDevicesWithoutGroup();
			},
			onRender: function () {
				if (!this.addDeviceTile) {
					var destination = App.getCurrentGateway() ? "setup/pairing" : "setup/gateway";

					this.addDeviceTile = new App.Consumer.Equipment.Views.AddNewTileView({
						i18nTextKey: "equipment.myEquipment.groups.addNewEquipment",
						size: "small",
						classes: "ungrouped-equipment-tile",
						clickDestination: destination
					});
				}

				var listContainer = this.$childViewContainer;

				if (!listContainer) {
					//$childViewContainer will be null if collection is empty.
					listContainer = this.$(".list-container");
				}

				listContainer.prepend(this.addDeviceTile.render().$el);
			},
			onDestroy: function () {
				this.addDeviceTile.destroy();
			}
		}).mixin([SalusView]);

		Views.GroupsTabView = Mn.LayoutView.extend({
			id: "bb-groups-tab",
			template: templates["equipment/myEquipment/groupsTab"],
			regions: {
				"groupsRegion": "#bb-groups-region",
				"devicesRegion": "#bb-devices-region"
			},
			onRender: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise(["groups"]).then(function (/*arrayOfData*/) {
					that.groupsRegion.show(new Views.Groups());
				});

				App.salusConnector.getDataLoadPromise(["groups", "devices"]).then(function (/*arrayOfData*/) {
					that.devicesRegion.show(new Views.Devices());
				});
			}
		}).mixin([SalusTabView]);
	});

	return App.Consumer.Equipment.Views.GroupsTabView;
});