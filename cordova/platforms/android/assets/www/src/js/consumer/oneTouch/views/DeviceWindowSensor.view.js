"use strict";


define([
	"app",
	"bluebird",
    "consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
],function (App, P, consumerTemplates, SalusPageMixin){
    App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {
        Views.DeviceWindowSensor = Mn.LayoutView.extend({
            className: "device-window-sensor-page container",
            template: consumerTemplates["oneTouch/deviceWindowSensor"],
            regions: {
                "windowOpenCheckboxRegion": ".bb-window-open-checkbox",
                "windowCloseCheckboxRegion": ".bb-window-close-checkbox",
                "equipmentListRegion": ".bb-equipment-available",
                "saveButtonRegion": ".bb-save-button",
                "cancelButtonRegion": ".bb-cancel-button"
            },
            initialize: function (options) {
                var that=this;
                
                _.bindAll(this, "handleFinishClick", "handleCancelClick");
                
                this.key=options.key;
                
                
                this.windowOpenCheckboxModel=new App.Consumer.Models.CheckboxViewModel({
                    name: "equipmentCheckbox",
                    nonKeyLabel: App.translate("equipment.thermostat.menus.modeLabels.lock"),
                    secondaryIconClass: "",
                    isChecked: false
                });
                
                this.windowCloseCheckboxModel=new App.Consumer.Models.CheckboxViewModel({
                    name: "equipmentCheckbox",
                    nonKeyLabel: App.translate("equipment.thermostat.menus.modeLabels.unlock"),
                    secondaryIconClass: "",
                    isChecked: false
                });
                
                
                this.windowOpenCheckbox=new App.Consumer.Views.CheckboxView({
                    model: this.windowOpenCheckboxModel
                });
                
                this.windowCloseCheckbox=new App.Consumer.Views.CheckboxView({
                    model: this.windowCloseCheckboxModel
                });
                
                this.listenTo(this.windowOpenCheckbox,"clicked:checkbox",function (model){
                    that.windowCloseCheckboxModel.set("isChecked",model.get("isChecked"));
                });
                
                this.listenTo(this.windowCloseCheckbox,"clicked:checkbox",function (model){
                    that.windowOpenCheckboxModel.set("isChecked",model.get("isChecked"));
                });
                
                
            },
            onRender: function (){
                var that=this;
                
                App.salusConnector.getDataLoadPromise(["devices", "groups"]).then(function () {
                    that.equipmentListRegion.show(new App.Consumer.Equipment.Views.EquipmentCollectionView({
						collection: App.salusConnector.getWindowSensorCollection(),
                        deviceType: "all"
					}));
                });
                
                this.windowOpenCheckboxRegion.show(this.windowOpenCheckbox);
                
                this.windowCloseCheckboxRegion.show(this.windowCloseCheckbox);
                
                this.saveButtonRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
					classes: "width100",
					buttonTextKey: "common.labels.save",
					clickedDelegate: that.handleFinishClick
				}));
                
                this.cancelButtonRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
					className: "btn btn-default width100",
					buttonTextKey: "common.labels.cancel",
					clickedDelegate: that.handleCancelClick
				}));
            },
            handleFinishClick: function(event, button){
                var windowSensors,keys,openRuleModel,closeRuleModel;
                if(this.windowOpenCheckbox.getIsChecked()){
                    windowSensors=[];
                    keys=this.equipmentListRegion.currentView.getCheckedItemKeys();
                    
                    _.each(keys,function (key){
                        windowSensors.push(App.salusConnector.getDeviceByKey(key));
                    });
                    
                    openRuleModel=new App.Models.RuleModel({dsn: App.getCurrentGatewayDSN()});
                    closeRuleModel=new App.Models.RuleModel({dsn: App.getCurrentGatewayDSN()});
                    
                    
                    openRuleModel.buildWindowOpenRule(windowSensors,App.salusConnector.getDeviceByKey(this.key));
                    closeRuleModel.buildWindowCloseRule(windowSensors,App.salusConnector.getDeviceByKey(this.key));
                    
                    this.saveButtonRegion.currentView.showSpinner();
                    
                    P.all([openRuleModel.add(),closeRuleModel.add()]).then(function (){
                        window.history.back();
                    });
                
                }
                
            },
            handleCancelClick: function (){
                window.history.back();
            }
        }).mixin([SalusPageMixin]);
    });
    
    return App.Consumer.OneTouch.Views.DeviceWindowSensor;
});