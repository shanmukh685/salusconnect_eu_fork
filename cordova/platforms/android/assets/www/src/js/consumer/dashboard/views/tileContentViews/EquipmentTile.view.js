"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/dashboard/views/tileContentViews/mixin.TileContentView"
], function (App, constants, consumerTemplates, TileContentViewMixin) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views, App, B, Mn) {
		Views.EquipmentTile  = {};

		Views.EquipmentTile.FrontView = Mn.ItemView.extend({
			className: "equipment-tile",
			template: consumerTemplates["dashboard/tile/equipmentTileFront"],
			attributes: {
				role: "button"
			},
			ui: {
				frontIcon: ".bb-equipment-icon"
			},
			onRender: function () {
				this.ui.frontIcon.css("background-image", "url(" + this.model.get("device_category_icon_url") + ")");
			},
			templateHelpers: function () {
				return {
					isLargeTile: this.isLargeTile
				};
			}
		}).mixin([TileContentViewMixin]);

		Views.EquipmentTile.BackView = Mn.ItemView.extend({
			className: "equipment-tile",
			template: consumerTemplates["dashboard/tile/tileBack"],
			ui: {
				backIcon: ".bb-equipment-icon"
			},
			onRender: function () {
				this.ui.backIcon.removeClass("hidden");
				this.ui.backIcon.css("background-image", "url(" + this.model.get("device_category_icon_url") + ")");
			}
		}).mixin([TileContentViewMixin]);
	});

	return App.Consumer.Dashboard.Views.EquipmentTile;
});
