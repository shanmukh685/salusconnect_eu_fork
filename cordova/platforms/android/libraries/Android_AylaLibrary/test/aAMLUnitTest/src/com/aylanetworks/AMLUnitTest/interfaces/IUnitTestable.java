/*
 * IUnitTestable.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 05/04/2015.
 * Copyright (c) 2015 Ayla Networks Inc. All rights reserved.
 * */


package com.aylanetworks.AMLUnitTest.interfaces;

public interface IUnitTestable {

	// All the preCondition work should be done here
	public void onResume();
	
	// Tests start here
	public void start();
	
	// All the postCondition work should be done here.
	public void onPause();
	
	// Handling the end of the test suite
	public void onEnd();
	
	// All the GC work should be done here.
	public void onDestroy();
	
	// If the test suite is in the middle of running.
	public boolean isRunning();
	
	// Setup result screen
	public void setResultScreen(ITestResultDisplayable screen);
	
	// Set end handling for the test suit.
	public void setEndHandler(Runnable r);
}// end of IUnitTestable interface         








