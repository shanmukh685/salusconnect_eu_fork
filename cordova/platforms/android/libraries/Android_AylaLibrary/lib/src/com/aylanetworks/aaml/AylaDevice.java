//
//  AylaDevice.java
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 8/15/12.
//  Copyright (c) 2012 Ayla Networks. All rights reserved.
//

package com.aylanetworks.aaml; 

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.json.JSONObject;

import com.aylanetworks.aaml.AylaLanMode.*;
import com.google.gson.annotations.Expose;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

//------------------------------------- AylaDevice --------------------------

class AylaLanModeConfigContainer {
	@Expose
	AylaLanModeConfig lanip;
}

class AylaLanModeConfig {
	@Expose
	Number lanipKeyId;
	@Expose
	String lanipKey;
	@Expose
	Number keepAlive;
	@Expose
	Number autoSync;
	@Expose
	String status  = "UnKnown";;
}

class AylaDeviceContainer {
	@Expose
	AylaDevice device;
}

public class AylaDevice extends AylaSystemUtils {

	// device properties retrievable from the service
	@Expose
	public String connectedAt;	// last time the device connected to the service
	@Expose
	public String connectionStatus;	// near realtime indicator of device to service connectivity. Values are "Online" or "OffLine"
	@Expose
	public String dsn;			// Unique Device Serial Number
	@Expose
	public Boolean hasProperties;	// Does this device have properties
	// id
	@Expose
	public String ip;			// public external WAN IP Address
	@Expose
	private Number key;			// Unique Device Service DB identifier
	@Expose
	public boolean lanEnabled;	// is LAN Mode enabled on the service
	@Expose
	public String lanIp;		// local WLAN IP Address
	@Expose
	public String lat;			// latitude coordinate
	@Expose
	public String lng;			// longitude coordinate
	@Expose
	public String mac;			// optional
	@Expose
	public String model;		// device model
	@Expose
	public String moduleUpdatedAt; // when this device last completed OTA
	@Expose
	public String oem;
	@Expose
	public String oemModel;		// OEM model of the device. Typically assigned by the template

	
	@Expose
	public String productClass;	// device product class
	@Expose
	public String productName;	// device product name, user assignable

	@Expose
	public String ssid; 		// ssid of the AP the device is connected to	
	@Expose
	public String swVersion;	// software version running on the device
	@Expose
	public Number userID;		// User Id who has registered this device
	@Expose
	public Number templateId;	// template Id associated with this device
	@Expose
	public String registrationType; // how to register this device. One of "Same-LAN", "Button-Push", "AP-Mode", "Display", "None"(OEM)
	@Expose
	public String deviceName;	// name of this device
	@Expose
	public AylaGrant grant;		// If present, it indicates the device is registered to another user - it has been shared with this user


	// derived device properties
	@Expose
	AylaLanModeConfig lanModeConfig;
	@Expose
	public AylaTimezone timezone;

	@Expose
	public String setupToken;
	@Expose
	public String registrationToken;

	@Expose
	public AylaProperty property;
	@Expose
	public AylaProperty[] properties;

	@Expose
	public AylaSchedule schedule;
	@Expose
	public AylaSchedule[] schedules;

	@Expose
	public AylaDatum datum;
	@Expose
	public AylaDatum datums[];
	
	@Expose
	public AylaShare share;
	@Expose
	public AylaShare shares[];
	
	@Expose
	public AylaDeviceNotification deviceNotification;
	@Expose
	public AylaDeviceNotification[] deviceNotifications;


	static boolean waitForDiscovery = false;

	// constructors
	public AylaDevice () {
		lanModeConfig = new AylaLanModeConfig();
		timezone = new AylaTimezone();
	}

	public AylaDevice(Number key, String dsn) {
		this();
		this.key = key;
		this.dsn = dsn;
	}

	// getters & setters
	public Number getKey() {
		return key;
	}

	public String getProductName() {
		return productName;
	}

	public String getModel() 	{
		return model;
	}
	
	// Constants
	public final static String kAylaDeviceTypeWifi = "Wifi";
	public final static String kAylaDeviceTypeGateway = "Gateway";
	public final static String kAylaDeviceTypeNode = "Node";

	public final static String kAylaDeviceClassNameGateway = "AylaDeviceGateway";
	public final static String kAylaDeviceClassNameNode = "AylaDeviceNode";
	public final static String kAylaDeviceClassName = "AylaDevice";
	
	public final static String kAylaDeviceJoinWindowDuration = "duration";


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		String NEW_LINE = System.getProperty("line.separator");

		result.append(this.getClass().getName() + " Object {" + NEW_LINE);
		result.append(" productName: " + productName + NEW_LINE);
		result.append(" key(Service DB ID): " + key + NEW_LINE);
		result.append(" model: " + model + NEW_LINE);
		result.append(" dsn: " + dsn + NEW_LINE );
		result.append(" oemModel: " + oemModel + NEW_LINE);
		result.append(" connectedAt: " + connectedAt + NEW_LINE);
		result.append(" mac: " + mac + NEW_LINE);
		result.append(" lanIp: " + lanIp + NEW_LINE);
		result.append(" templateId: " + templateId + NEW_LINE);
		result.append(" registrationType" + registrationType + NEW_LINE);
		result.append(" setupToken" + setupToken + NEW_LINE );
		result.append(" registrationToken " + registrationToken + NEW_LINE);
		result.append("}");
		System.out.println(result);
		return result.toString();
	}

	// -------------------------- Change Device -----------------------
	/**
	 * This instance method supports to update module information.
	 * 
	 * @param mHandle is where result would be returned.
	 * @param callParams allows for specifying module information required to be changed.
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	public AylaRestService update(Map<String, String>callParams) {
		return update(null, callParams, true) ;
	}
	public AylaRestService update(Handler mHandle, Map<String, String>callParams) {
		return update(mHandle, callParams, false) ;
	}
	public AylaRestService update(Handler mHandle, Map<String, String>callParams, Boolean delayExecution) 
	{
		// PUT https://ads-dev.aylanetworks.com/apiv1/devices/<dev_key>.json
		int errCode = AML_USER_INVALID_PARAMETERS;
		Number devKey = this.getKey().intValue(); // Handle gson LazilyParsedNumber
		String url = String.format(Locale.getDefault(),"%s%s%d%s", deviceServiceBaseURL(), "devices/", devKey, ".json");
		AylaRestService rs = null;

		JSONObject deviceValues = new JSONObject();
		JSONObject errors = new JSONObject();
		String paramKey, paramValue;

		try {
			// test validity of objects
			paramKey = "productName";
			paramValue = (String)callParams.get(paramKey);
			if (TextUtils.isEmpty(paramValue)) {
				errors.put(paramKey, "can't be blank"); // use delete() & signUp()
			}
			deviceValues.put("product_name", paramValue);

			// return if errors in required fields
			if(errors.length() != 0) {
				rs = new AylaRestService(mHandle, ERR_URL, AylaRestService.UPDATE_DEVICE);
				saveToLog("%s, %s, %s:%s, %s", "E", "AylaDevice", ERR_URL, errors.toString(), "update");
				returnToMainActivity(rs, errors.toString(), errCode, 0, false);
				return rs;
			}

			// All good, update the device
			rs = new AylaRestService(mHandle, url, AylaRestService.UPDATE_DEVICE);
			saveToLog("%s, %s, %s:%s, %s", "I", "AylaDevice", "device.productName", this.productName, "update");
			String jsonDeviceContainer = new JSONObject().put("device", deviceValues).toString();		

			rs.setEntity(jsonDeviceContainer);
			if (delayExecution == false) {
				rs.execute(); // result returned through stripContainer
			}
			return rs;

		} catch (Exception e) {
			rs = new AylaRestService(mHandle, ERR_URL, AylaRestService.UPDATE_DEVICE);
			saveToLog("%s, %s, %s:%s, %s", "E", "AylaDevice", "exception", e.getCause(), "update");
			returnToMainActivity(rs, errors.toString(), AML_GENERAL_EXCEPTION, 0, false);
		}

		return rs;
	}
	/**
	 * 
	 * @param mHandle
	 * @param delayExecution
	 * @return
	 */
	protected static AylaRestService getNewDeviceConnected(Handler mHandle, String dsn, String setupToken, Boolean delayExecution) {
		//String url = "http://ads-dev.aylanetworks.com/apiv1/devices/connected.json";
		String baseUrl = deviceServiceBaseURL();
		if (AylaSystemUtils.secureSetup == NO) {
			baseUrl = deviceServiceBaseURL().replace("https","http");
		}
		String url = String.format("%s%s%s", baseUrl, "devices/connected", ".json");
		AylaRestService rs = new AylaRestService(mHandle, url, AylaRestService.GET_NEW_DEVICE_CONNECTED);
		String delayedStr = (delayExecution == true) ? "true" : "false";
		saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "AylaSetup", "url", url, "delayedExecution", delayedStr, "AylaDevice.getNewDeviceConnected");

		rs.addParam("dsn", dsn);
		rs.addParam("setup_token", setupToken);

		saveToLog("%s, %s, %s:%s, %s:%s ,%s", "I", "AylaSetup", "setupToken", "setupToken", "dsn", dsn, "AylaDevice.getNewDeviceConnected");
		if (delayExecution == false) {
			rs.execute(); 
		}
		return rs;
	}

	/**
	 * These class methods get one or more registered devices from the Ayla Cloud Service. If the application has been LAN Mode enabled, the devices 
	 * are read from cache, rather than the Ayla field service.
	 * 
	 * @param mHandle is where async results are returned. If null, restServiceObject.execute() provides synchronous results
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	public static AylaRestService getDevices() {
		return getDevices(null, true);
	}
	public static AylaRestService getDevices(Handler mHandle) {
		return getDevices(mHandle, false);
	}
	public static AylaRestService getDevices(Handler mHandle, Boolean delayExecution) {
		AylaRestService rs = null;
		String savedJsonDeviceContainers = "";

		// read the devices from storage, returns "" if no cached values
		savedJsonDeviceContainers = AylaCache.get(AML_CACHE_DEVICE);


		// get devices from the service
		if ( AylaReachability.isCloudServiceAvailable()) {
			if (AylaSystemUtils.slowConnection == AylaNetworks.YES) {
				if (!TextUtils.isEmpty(savedJsonDeviceContainers)) {
					// Use cached values
					try {
						String jsonDevices = stripContainers(savedJsonDeviceContainers, AylaRestService.GET_DEVICES);
						rs = new AylaRestService(mHandle, "GetDevicesStorageLanMode", AylaRestService.GET_DEVICES_LANMODE);
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaDevices", "fromStorage", "true", "getDevicesStorage");
						returnToMainActivity(rs, jsonDevices, 203, 0, delayExecution);
					} catch (Exception e) {
						AylaSystemUtils.saveToLog("%s %s %s:%s %s", "E", "AylaDevices", "exception", e.getCause(), "getDevicesStorage_lanMode");
						e.printStackTrace();
					}			
				} else {
					// query field service
					// String url = "http://ads-dev.aylanetworks.com/apiv1/devices.json";
					String url = String.format("%s%s%s", AylaSystemUtils.deviceServiceBaseURL(), "devices", ".json");
					rs = new AylaRestService(mHandle, url, AylaRestService.GET_DEVICES);
					String delayedStr = (delayExecution == true) ? "true" : "false";
					saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "AylaDevices", "url", url, "delayedExecution", delayedStr, "getDevicesService");
					if (delayExecution == false) {
						rs.execute(); 
					}
				}
			} else {
				// query field service
				// String url = "http://ads-dev.aylanetworks.com/apiv1/devices.json";
				String url = String.format("%s%s%s", AylaSystemUtils.deviceServiceBaseURL(), "devices", ".json");
				rs = new AylaRestService(mHandle, url, AylaRestService.GET_DEVICES);
				String delayedStr = (delayExecution == true) ? "true" : "false";
				saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "AylaDevices", "url", url, "delayedExecution", delayedStr, "getDevicesService");
				if (delayExecution == false) {
					rs.execute(); 
				}
			}
		}

		// service is not reachable
		else 
			if (AylaReachability.isWiFiConnected(null) && !TextUtils.isEmpty(savedJsonDeviceContainers))
			{
				// use cached values
				try {
					String jsonDevices = stripContainers(savedJsonDeviceContainers, AylaRestService.GET_DEVICES);

					rs = new AylaRestService(mHandle, "GetDevicesStorageLanMode", AylaRestService.GET_DEVICES_LANMODE);
					saveToLog("%s, %s, %s:%s, %s", "I", "AylaDevices", "fromStorage", "true", "getDevicesStorage");
					returnToMainActivity(rs, jsonDevices, 203, 0, delayExecution);
				} catch (Exception e) {
					AylaSystemUtils.saveToLog("%s %s %s:%s %s", "E", "AylaDevices", "exception", e.getCause(), "getDevicesStorage_lanMode");
					e.printStackTrace();
				}
			}

		// no devices
			else { 
				if (AylaCache.cacheEnabled(AML_CACHE_DEVICE) == true && !TextUtils.isEmpty(savedJsonDeviceContainers)) {
					// use cached values
					try {
						String jsonDevices = stripContainers(savedJsonDeviceContainers, AylaRestService.GET_DEVICES);

						rs = new AylaRestService(mHandle, "GetDevicesStorageLanMode", AylaRestService.GET_DEVICES_LANMODE);
						saveToLog("%s, %s, %s:%s, %s", "I", "AylaDevices", "fromStorage", "true", "getDevicesStorage");
						returnToMainActivity(rs, jsonDevices, 203, 0, delayExecution);
					} catch (Exception e) {
						AylaSystemUtils.saveToLog("%s %s %s:%s %s", "E", "AylaDevices", "exception", e.getCause(), "getDevicesStorage_lanMode");
						e.printStackTrace();
					}
				} else {
					rs = new AylaRestService(mHandle, "GetDevicesStorageLanMode", AylaRestService.GET_DEVICES_LANMODE);
					saveToLog("%s, %s, %s:%s, %s", "I", "AylaDevices", "devices", "null", "getDevices");
					returnToMainActivity(rs, null, 404, 0, delayExecution);
				}
			}

		return rs;
	}
	
	
	protected static String stripContainers(String jsonDeviceContainers, int method ) throws Exception {
		int count = 0;
		String jsonDevices = "";
		AylaDevice[] devices = null;

		try {
			AylaDeviceContainer[] deviceContainers = AylaSystemUtils.gson.fromJson(jsonDeviceContainers, AylaDeviceContainer[].class);
			devices = new AylaDevice[deviceContainers.length];
			for (AylaDeviceContainer deviceContainer : deviceContainers) {
				devices[count]= deviceContainer.device;
				count++;
			}

			if (count > 0) {
				lanModeEnable(devices);  // enable devices for lan mode access
				AylaCache.save(AML_CACHE_DEVICE, jsonDeviceContainers); // save devices to cache
			}
			AylaSystemUtils.saveToLog("%s %s %s:%s %s", "I", "Devices", "count", count, "stripContainers");

			jsonDevices = AylaSystemUtils.gson.toJson(devices, AylaDevice[].class);
			return jsonDevices;

		} catch (Exception e) {
			if (jsonDeviceContainers == null) {
				jsonDeviceContainers = "null";
			}
			AylaSystemUtils.saveToLog("%s %s %s:%s %s:%s %s", "E", "Devices", "count", count, "jsonDeviceContainers", jsonDeviceContainers, "stripContainers");
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * This instance method will instantiate a new registered device object from the Ayla Cloud Service and retrieve its associated properties.
	 * Use this call only if additional device detail is required. In almost all cases, using the getProperties method is the preferred and more.
	 * 
	 * @param mHandle is where async results are returned. If null, restServiceObject.execute() provides synchronous results
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	public AylaRestService getDeviceDetail() {
		return getDeviceDetail(null, true);
	}
	public AylaRestService getDeviceDetail(Handler mHandle) {
		return getDeviceDetail(mHandle, false);
	}
	public AylaRestService getDeviceDetail(Handler mHandle, Boolean delayExecution) {
		Number devKey = this.getKey().intValue(); // Handle gson LazilyParsedNumber;
		// String url = "http://ads-dev.aylanetworks.com/apiv1/devices/###.json";
		String url = String.format(Locale.getDefault(),"%s%s%d%s", deviceServiceBaseURL(), "devices/", devKey, ".json");
		AylaRestService restService = new AylaRestService(mHandle, url, AylaRestService.GET_DEVICE_DETAIL);
		String delayedStr = (delayExecution == true) ? "true" : "false";
		saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "Devices", "url", url, "delayedExecution", delayedStr, "getDeviceDetail");

		if (delayExecution == false) {
			restService.execute(); 
		}
		return restService;
	}

	
	/**
	 * Removes device container ("device:") from device service API response (see AylaExecuteRequest.commit())
	 * Used by device service calls returning information about a single device (vs an array of devices)
	 * 
	 * @param jsonDeviceContainer - AylaDevice object with key identifier
	 * @param method - AylaRestService requestId
	 * @return AylaDevice in JSON format
	 * @throws Exception
	 */
	protected static String stripContainer(String jsonDeviceContainer, int method ) throws Exception {
		String jsonDevice = "";
		AylaDevice device = null;
		
		try {
			AylaDeviceContainer deviceContainer = AylaSystemUtils.gson.fromJson(jsonDeviceContainer, AylaDeviceContainer.class);
			device = deviceContainer.device;

			if ( (method == AylaRestService.GET_DEVICE_DETAIL || method == AylaRestService.UPDATE_DEVICE) &&
					device != null )
			{
				// device.lanModeEnable();
				AylaLanMode.device = device;
			}

			jsonDevice = AylaSystemUtils.gson.toJson(device,AylaDevice.class);
			AylaSystemUtils.saveToLog("%s %s %s", "I", "Devices", "stripContainer");
			return jsonDevice;
		} catch (Exception e) {
			AylaSystemUtils.saveToLog("%s %s %s:%s %s", "E", "Devices", "jsonDeviceContainer", jsonDeviceContainer, "stripContainer");
			e.printStackTrace();
			throw e;
		}
	}
	
	/**
	 * This method will do factory reset for the current device. 
	 * There are no call parameters required for this method at this time, so supply null for now.
	 * @param callParams not required: null
	 */
	public AylaRestService factoryReset(Map<String, Object> callParams) {
		return factoryReset(null, callParams, true);
	}
	public AylaRestService factoryReset(Handler mHandle, Map<String, Object> callParams) {
		return factoryReset(mHandle, callParams, false);
	}
	public AylaRestService factoryReset(Handler mHandle, Map<String, Object> callParams, Boolean delayExecution) {
			
		Number devKey = this.getKey().intValue(); // Handle gson LazilyParsedNumber;
		// String url = "http://ads-dev.aylanetworks.com/apiv1/devices/<devKey>/cmds/factory_reset.json";
		String url = String.format(Locale.getDefault(),"%s%s%d%s", deviceServiceBaseURL(), "devices/", devKey, "/cmds/factory_reset.json");
		AylaRestService restService = new AylaRestService(mHandle, url, AylaRestService.PUT_DEVICE_FACTORY_RESET);
		String delayedStr = (delayExecution == true) ? "true" : "false";
		saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "Devices", "url", url, "delayedExecution", delayedStr, "factoryReset");

		if (delayExecution == false) {
			restService.execute(); 
		}
		return restService;
	}

	
	// TODO: Move to a common utils class
	protected static void returnToMainActivity(AylaRestService rs, String thisJsonResults, int thisResponseCode, int thisSubTaskId, Boolean delayExecution) {
		rs.jsonResults = thisJsonResults;
		rs.responseCode = thisResponseCode;
		rs.subTaskFailed = thisSubTaskId;
		if (delayExecution == false) {
			rs.execute();
		}
	}

	// --------------------- Pass through to device helper classes -----------------------------------
	
	
	
	// --------------------- Properties pass-through methods -----------------------------------------
	/**
	 * Gets all properties summary objects associated with the device from Ayla device Service. Use getProperties when ordering is not important.
	 * 
	 * @param mHandle is where async results are returned. If null, restServiceObject.execute() provides synchronous results
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	public AylaRestService getProperties() {	// sync, get all properties
		return AylaProperty.getProperties(null, this, null, true);
	}
	public AylaRestService getProperties(Map<String, String> callParams) {	// sync, get some properties
		return AylaProperty.getProperties(null, this, callParams, true);
	}
	public AylaRestService getProperties(Handler mHandle) {	// async, get all properties
		AylaRestService rs = AylaProperty.getProperties(mHandle, this, null, false); 
		return rs;
	}
	public AylaRestService getProperties(Handler mHandle, Map<String, String> callParams) {	// async get some properties async
		AylaRestService rs = AylaProperty.getProperties(mHandle, this, callParams, false); 
		return rs;
	}
	public AylaRestService getProperties(Handler mHandle, Boolean delayExecution) {	// async get all properties
		AylaRestService rs = AylaProperty.getProperties(mHandle, this, null, delayExecution); 
		return rs;
	}
	public AylaRestService getProperties(Handler mHandle, Map<String, String> callParams, Boolean delayExecution) {	// async get some properties
		AylaRestService rs = AylaProperty.getProperties(mHandle, this, callParams, delayExecution); 
		return rs;
	}

	/**
	 * Device Registration provides a way to easily register a device once it has successfully completed the Setup process. Devices must be registered
	 * before they can be accessed by the Device Service methods.
	 * 
	 * @param mHandle is where async results are returned. If null, restServiceObject.execute() provides synchronous results
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	public AylaRestService registerNewDevice() {	// sync
		AylaRestService rs = AylaRegistration.registerNewDevice(null, this);
		return rs;
	}
	public AylaRestService registerNewDevice(Handler mHandle) {	// async
		AylaRestService rs = AylaRegistration.registerNewDevice(mHandle, this);
		return rs;
	}

	/**
	 * This method will unregister a device from a user account.
	 * 
	 * @param mHandle is where async results are returned. If null, restServiceObject.execute() provides synchronous results
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	public AylaRestService unregisterDevice() {
		return AylaRegistration.unregisterDevice(null, this);
	}
	public AylaRestService unregisterDevice(Handler mHandle) {
		AylaRestService rs = AylaRegistration.unregisterDevice(mHandle, this);
		return rs;
	}

	// ----------------------------- Schedule Pass Through Methods ----------------------------
	/**
	 * This updateSchedule method is used to update/change schedule object and associated Schedule Action properties. When using the Full Template Schedule
	 * Model,(schedules and Actions are pre-created in the OEM template), this method will PUT the data to existing schedule and action instances passed in 
	 * as parameters. When using the Dynamic Template Schedule Model, (schedules are precreated in the OEM template, Schedule Actions are dynamically created
	 * and deleted), this method will create and delete the Actions as required if newly allocated scheduleAction object(s) are passed in as parameters.
	 * 
	 * @param mHandle is where async results are returned. If null, restServiceObject.execute() provides synchronous results
	 * @param schedule is the current schedule object set to desired values.
	 * @param actions is the array of new actions updated to current schedule
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	public AylaRestService updateSchedule(AylaSchedule schedule, AylaScheduleAction[] actions) {
		return schedule.update(null, this, actions);
	}
	public AylaRestService updateSchedule(Handler mHandle, AylaSchedule schedule, AylaScheduleAction[] actions) {
		return schedule.update(mHandle, this, actions);
	}

	/**
	 * This method results in all schedules for a given device object being return to successBlock. Each AylaSchedule array member instance includes only
	 * the schedule properties and not the associated Schedule Actions. This method is typically used to provide a top-level listing of available schedules
	 * from which the end user selects.
	 * 
	 * @param mHandle is where async results are returned. If null, restServiceObject.execute() provides synchronous results
	 * @param callParams must contain required parameter(s) by this method. Please read the mobile library document for details.
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	public AylaRestService getAllSchedules(Map<String, String> callParams) {
		return getAllSchedules(null, callParams, true);
	}
	public AylaRestService getAllSchedules(Handler mHandle, Map<String, String> callParams) {
		return getAllSchedules(mHandle, callParams, false);
	}
	public AylaRestService getAllSchedules(Handler mHandle, Map<String, String> callParams, boolean delayExecution) {
		return AylaSchedule.getAll(mHandle, this, callParams, delayExecution);
	}

	/**
	 * The method results in the schedule matching the given name being returned to the handler. The AylaSchedule instance includes the schedule properties
	 * and the associated Schedule Actions. This method is typically used to provide complete schedule information for a top-level schedule selected from a
	 * list populated by the getAllSchedules method.
	 * 
	 * @param mHandle is where async results are returned. If null, restServiceObject.execute() provides synchronous results
	 * @param callParams must contain required parameter(s) by this method. Please read the mobile library document for details.
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	public AylaRestService getScheduleByName(Map<String, String> callParams) {
		return getScheduleByName(null,  callParams, true);
	}
	public AylaRestService getScheduleByName(Handler mHandle, Map<String, String> callParams) {
		return getScheduleByName(mHandle,  callParams, false);
	}
	public AylaRestService getScheduleByName(Handler mHandle, Map<String, String> callParams, boolean delayExecution) {
		return AylaSchedule.getByName(mHandle, this, callParams, delayExecution);
	}
	
	/**
	 * The clearSchedule method will delete the Schedule Actions associated with the Schedule instance and also set the schedule.active value to false. Consider 
	 * the clear method a virtual delete method for the Dynamic Action Schedule Model. DO NOT use clear when implementing the Full Template model as it will 
	 * delete the Actions. Instead, simply set schedule.active (and optionally the associated scheduleAction[].active values) to false using the updateSchedule 
	 * method.
	 * 
	 * @param mHandle is where async results are returned. If null, restServiceObject.execute() provides synchronous results
	 * @param schedule is the Schedule to be cleared.
	 * @param delayExecution set to true to setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	public AylaRestService clearSchedule(AylaSchedule schedule) {
		return schedule.clear(null, true);
	}
	public AylaRestService clearSchedule(Handler mHandle, AylaSchedule schedule) {
		return schedule.clear(mHandle, false);
	}
	public AylaRestService clearSchedule(Handler mHandle, AylaSchedule schedule, boolean delayExecution) {
		return schedule.clear(mHandle, delayExecution);
	}

	// ----------------------------- Timezone Pass Through Methods ----------------------------
	/**
	 * Posts the time zone associated with the device to the Ayla device Service.
	 * device.timeZone:
	 *     utc_offset (required): string which specifies utc offset. Format must be +HH:MM or -HH:MM. For example +05:00 or -03:00.
	 *     dst (optional): boolean which specifies if the location follows DST. Default: false.
	 *     dst_active (optional): booelan which specifies if DST is currently active. Default: false.
	 *     dst_next_change_date (optional): string which specifies next DST state change from active/inactive OR from inactive/active. Format must be yyyy-mm-dd
	 *     tz_id (optional): String identifier for the timezone. eg: "America/New_York".
	 *         NOTE: If tz_id is present, it must correlate with the utc_offset, else the POST will be rejected.
	 *
	 * @param mHandle is where result will be returned.
	 * @param device is the target object
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	public AylaRestService createTimezone() {
		return timezone.create(null, this, true);
	}
	public AylaRestService createTimezone(Handler mHandle) {
		return timezone.create(mHandle, this, false);
	}
	public AylaRestService createTimezone(Handler mHandle, Boolean delayExecution) {
		return timezone.create(mHandle, this, false);
	}
	
	
	/**
	 * Update the timezone ID for this device
	 * @param device is the target object
	 *     tzId: (required): String identifier for the timezone. eg: "America/New_York".
	 *     NOTE: DST attributes are updated, based on whether the timezone has DST or not. 
	 * @return
	 */
	public AylaRestService updateTimezone() {
		return timezone.update(null, this, true);
	}
	public AylaRestService updateTimezone(Handler mHandle) {
		return timezone.update(mHandle, this, false);
	}
	public AylaRestService updateTimezone(Handler mHandle, Boolean delayExecution) {
		return timezone.update(mHandle, this, false);
	}
	
	
	/**
	 * Get the timezone information for this device
	 * 
	 * @param mHandle is where async results are returned. If null, restServiceObject.execute() provides synchronous results
	 * @param device is the target object
	 * @param delayExecution delayExecution set to true to setup this call but have it execute on an external event
	 * @return AylaRestService object
	 */
	public AylaRestService getTimezone() {
		return timezone.get(null, this, true);
	}
	public AylaRestService getTimezone(Handler mHandle) {
		return timezone.get(mHandle, this, false);
	}
	public AylaRestService getTimezone(Handler mHandle, Boolean delayExecution) {
		return timezone.get(mHandle, this, delayExecution);
	}

	
	// ---------------------- Device Datum pass-through methods ------------------------
	public AylaRestService createDatum(AylaDatum deviceDatum) {
		return deviceDatum.create(this);
	}
	public AylaRestService createDatum(Handler mHandle, AylaDatum deviceDatum) {
		return deviceDatum.create(mHandle, this);
	}
	public AylaRestService updateDatum(AylaDatum deviceDatum) {
		return deviceDatum.update(this);
	}
	public AylaRestService updateDatum(Handler mHandle, AylaDatum deviceDatum) {
		return deviceDatum.update(mHandle, this);
	}
	public AylaRestService getDatumWithKey(String key) {
		return AylaDatum.getWithKey(this, key);
	}
	public AylaRestService getDatumWithKey(Handler mHandle, String key) {
		return AylaDatum.getWithKey(mHandle, this, key);
	}
	public AylaRestService getDatum(Map<String, ArrayList<String>> callParams) {
		return AylaDatum.get(this, callParams);
	}
	public AylaRestService getDatum(Handler mHandle, Map<String, ArrayList<String>> callParams) {
		return AylaDatum.get(mHandle, this, callParams);
	}
	public AylaRestService deleteDatum(AylaDatum deviceDatum) {
		return deviceDatum.delete(this);
	}
	public AylaRestService deleteDatum(Handler mHandle, AylaDatum deviceDatum) {
		return deviceDatum.delete(mHandle, this);
	}

	// ---------------------- Device Share pass-through methods ------------------------
	// Create an owned device share
	public AylaRestService createShare(AylaShare deviceShare) {
		return deviceShare.create(this);
	}
	public AylaRestService createShare(Handler mHandle, AylaShare deviceShare) {
		return deviceShare.create(mHandle, this);
	}
	// update an owned device share
	public AylaRestService updateShare(AylaShare deviceShare) {
		return deviceShare.update();
	}
	public AylaRestService updateShare(Handler mHandle, AylaShare deviceShare) {
		return deviceShare.update(mHandle);
	}
	// get an owned or received share for a given id
	public AylaRestService getShare(String id) {
		return AylaShare.getWithId(null, id);
	}
	public AylaRestService getShare(Handler mHandle, String id) {
		return AylaShare.getWithId(mHandle, id);
	}
	// get all owned shares for a given device
	public AylaRestService getShares() {
		return AylaShare.get(null, this, null);
	}
	public AylaRestService getShares(Map<String, String> callParams) {
		return AylaShare.get(null, this, callParams);
	}
	public AylaRestService getShares(Handler mHandle) {
		return AylaShare.get(mHandle, this, null);
	}
	public AylaRestService getShares(Handler mHandle, Map<String, String> callParams) {
		return AylaShare.get(mHandle, this, callParams);
	}
	// get all owned device shares for the current user
	static public AylaRestService getAllShares() {
		AylaDevice devObj = new AylaDevice();
		return AylaShare.getReceives(null, devObj, null);
	}
	static public AylaRestService getAllShares(Handler mHandle) {
		AylaDevice devObj = new AylaDevice();
		return AylaShare.getReceives(mHandle, devObj, null);
	}
	// delete an owned or received device share
	public AylaRestService deleteShare(AylaShare deviceShare) {
		return deviceShare.delete();
	}
	public AylaRestService deleteShare(Handler mHandle, AylaShare deviceShare) {
		return deviceShare.delete(mHandle);
	}

	// ---------------------- Device Received Share pass-through methods ------------------------
	// get received shares for this device.dsn
	public AylaRestService getReceivedShares() {
		return AylaShare.getReceives(null, this, null);
	}
	public AylaRestService getReceivedShares(Handler mHandle) {
		return AylaShare.getReceives(mHandle, this, null);
	}
	// get all received device shares for the current user
	static public AylaRestService getAllReceivedShares() {
		AylaDevice devObj = new AylaDevice();
		return AylaShare.getReceives(null, devObj, null);
	}
	static public AylaRestService getAllReceivedShares(Handler mHandle) {
		AylaDevice devObj = new AylaDevice();
		return AylaShare.getReceives(mHandle, devObj, null);
	}
	
	// ---------------------- Device Notification pass-through methods ------------------------
	/**
	 * Post a new device notification associated with input param device. See section Device Service - Device Notifications in aAyla Mobile Library document for details.
	 * @param mHandle is where result would be returned.
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event.
   	 * @return AylaRestService object
	 */
	public AylaRestService createNotification(AylaDeviceNotification deviceNotification) {
		return deviceNotification.createNotification(null, this, true);
	}
	public AylaRestService createNotification(Handler mHandle, AylaDeviceNotification deviceNotification) {
		return deviceNotification.createNotification(mHandle, this, false);
	}
	public AylaRestService createNotification(Handler mHandle, AylaDeviceNotification deviceNotification, Boolean delayExecution) {
		return deviceNotification.createNotification(mHandle, this, delayExecution);
	}
	/**
	 * Put a device notification associated with input param device. See section Device Service - Device Notifications in aAyla Mobile Library document for details.
	 * @param mHandle is where result would be returned.
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event.
   	 * @return AylaRestService object
	 */
	public AylaRestService updateNotification(AylaDeviceNotification deviceNotification) {
		return deviceNotification.updateNotification(null, true);
	}
	public AylaRestService updateNotification(Handler mHandle, AylaDeviceNotification deviceNotification) {
		return deviceNotification.updateNotification(mHandle, false);
	}
	public AylaRestService updateNotification(Handler mHandle, AylaDeviceNotification deviceNotification, Boolean delayExecution) {
		return deviceNotification.updateNotification(mHandle,  delayExecution);
	}
	/**
	 * Get all the device notifications associated with the device.
	 * @param mHandle is where result would be returned.
	 * @param callParams is not used. Set to null for now.
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event.
   	 * @return AylaRestService object
	 */
	public AylaRestService getNotifications(Map<String, String> callParams) {
		return AylaDeviceNotification.getNotifications(null, this, callParams, true);
	}
	public AylaRestService getNotifications(Handler mHandle, Map<String, String> callParams) {
		return AylaDeviceNotification.getNotifications(mHandle, this, callParams, false);
	}
	public AylaRestService getNotifications(Handler mHandle, Map<String, String> callParams, Boolean delayExecution) {
		return AylaDeviceNotification.getNotifications(mHandle, this, callParams, delayExecution);
	}
	/**
	 * Destroy a dedicated device notification.
	 * @param mHandle is where result would be returned.
	 * @param devNotification is the device notification to be destroyed.
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event.
   	 * @return AylaRestService object
	 */

	public AylaRestService destroyNotification(AylaDeviceNotification deviceNotification) {
		return deviceNotification.destroyNotification(null, true);
	}
	public AylaRestService destroyNotification(Handler mHandle, AylaDeviceNotification deviceNotification) {
		return deviceNotification.destroyNotification(mHandle, false);
	}
	public AylaRestService destroyNotification(Handler mHandle, AylaDeviceNotification deviceNotification, Boolean delayExecution) {
		return deviceNotification.destroyNotification(mHandle, delayExecution);
	}
	
	// ------------------------ Helper Methods ---------------------
	
	/**
	 * @return true if the registered/currentUser is the owner of this device, false otherwise, 
	 * like if this device has been shared with the current user.
	 * */
	public boolean amOwner() {
		boolean amOwner = false;
		if (this.grant == null) {
			amOwner = true;
		}
		return amOwner;
	}
	
	//TODO: this does not make any sense, remove it after verifying no app would be impacted.
	@Deprecated
	public boolean isDevice() {
		return true;
	}
	
	public boolean isWifi() {
		return (this instanceof AylaDeviceGateway || this instanceof AylaDeviceNode) ? false : true;
	}
	
	public boolean isGateway() {
		return (this instanceof AylaDeviceGateway);
	}
	
	public boolean isNode() {
		return (this instanceof AylaDeviceNode);
	}
	
	

	
	// ----------------------- Lan Mode Methods --------------------

	/**
	 * This method enables direct communication with the device after the application/activity has been LAN enabled. Call this message before any other
	 * AylaDevice methods to leverage direct communication. If the direct communication with the device is determined, a standard SUCCESS/FAILURE message is
	 * sent to the AylaLanMode notification handler. Subsequent calls to get property values should wait until LAN Mode enablement has been determined. If
	 * successful direct communication with the device is established, the receipt of a SUCCESS message by the notification handler will signal property changes
	 * from the device. The notification is generic and does not specify the nature of the change. Therefore, the application should immediately perform
	 * getProperties to assess the impact of the changes. See section LAN Mode Support of aAyla Mobile Library document for details.
	 */
	public void lanModeEnable() {
		AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaDevice", "dsn", this.dsn, "lanModeEnable");
		if (AylaSystemUtils.lanModeState == lanMode.DISABLED) {
			AylaSystemUtils.saveToLog("%s, %s, %s, %s", "I", "AylaDevice", "lanModeState:DISABLED", "lanModeEnable");
			return;
		}

		// disable current device
		if (AylaLanMode.savedLanModeDevice != null && AylaLanMode.device != null) {
			if (!TextUtils.equals(AylaLanMode.savedLanModeDevice.dsn, this.dsn)) {
				//lanModeDisable(); // kill session

				// Initialize queues
				AylaLanMode.clearSendQueue();			// queue commands for the device
				AylaLanMode.clearCommandsOutstanding(); // look up async commands response
			}
		}

		// assign new device
		AylaLanMode.device = this;
		// did enable lan mode for current device
		this.didEnableLanMode();
		String localHostName = String.format("%s%s", this.dsn, ".local");
		if (AylaLanMode.device.lanEnabled == true && AylaReachability.isWiFiConnected(null)) {
			AylaReachability.setDeviceReachability(AylaNetworks.AML_REACHABILITY_UNKNOWN);
			lanModeEnableContinue(AylaLanMode.discovery, localHostName);
		} else {
			lanModeSessionFailed();	// device is not reachable, notif
		}
	}

	private void lanModeEnableContinue(AylaDiscovery discovery, String localHostName) {
		AylaLanMode.discoveredLanIp = null;
		waitForDiscovery = true;
		discovery.queryDeviceIpAddress(localHostName);
	}

	// Called from AylaDiscovery
	protected static void lanModeEnableContinued(Boolean timedOut) {
		
		if (waitForDiscovery) { // waiting for discovery to return. Only need one value
			waitForDiscovery = false;
			if (AylaLanMode.discoveredLanIp != null && timedOut == false) { // we have a discovery
				if (AylaLanMode.device.lanIp != null) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaDevice", "AylaLanMode.device.LanIp", AylaLanMode.device.lanIp, "lanModeEnablContinued");
				}
				if (!TextUtils.equals(AylaLanMode.discoveredLanIp, AylaLanMode.device.lanIp)) {
					AylaLanMode.device.updateDevicesCacheLanIp(AylaLanMode.discoveredLanIp);
					AylaLanMode.device.lanIp = AylaLanMode.discoveredLanIp;
					AylaLanMode.discoveredLanIp = null;
				}
			}

			boolean waitForResults = true;
			AylaReachability.determineServiceReachability(waitForResults); // determine connectivity, reachablity, and notify registered handlers,  v3.00a
			AylaLanMode.getLanModeConfig();
		}
	}

	private static void lanModeEnable(AylaDevice[] devices) {
		AylaSystemUtils.saveToLog("%s, %s, %s, %s", "I", "AylaDevice", "entry", "lanModeEnables");
		if (AylaSystemUtils.lanModeState != lanMode.DISABLED) {
			if (devices != null) {
				// update gateway nodes from devices list
				// find gateways in devices list
				ArrayList<AylaDeviceGateway> gatewayList = new ArrayList<AylaDeviceGateway>();
				for(AylaDevice device : devices) {
					if(device.isGateway()) {
						gatewayList.add((AylaDeviceGateway)device);
					}
				}
				
				//update gateways' node list
				if(gatewayList.size() > 0) {
					for(AylaDeviceGateway gateway : gatewayList) {
						gateway.updateNodesFromDeviceList(devices, true);
					}
				}
				
				//TODO: next two lines should be changed:
				// simply replace devices will remove current information for devices.
				// should not change current AylaLanMode.device, this will replace current lan device without notifying lib/app
				AylaLanMode.devices = devices;
				//AylaLanMode.device = devices[0]; // to be removed
			} else {
				saveToLog("%s, %s, %s:%s, %s", "E", "AylaDevice", "lanModeDevice.devices", "null", "lanModeEnables");
			}
		} else {
			AylaSystemUtils.saveToLog("%s, %s, %s, %s", "I", "AylaDevice", "lanModeState:DISABLED", "lanModeEnables");
		}
	}

	/**
	 * This method is called when LAN mode connection to LME device is no longer required. Then library will stop responding any message from or to this device.
	 * All requests will be sent to service after this method is called. See section LAN Mode Support of aAyla Mobile Library document for details.
	 */
	public void lanModeDisable() {
		AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaDevice", "dsn", this.dsn, "lanModeDisable");
		if (AylaSystemUtils.lanModeState != lanMode.DISABLED && AylaLanMode.device != null) {
			if (AylaLanMode.device.properties != null) {
				try {
					// write current property values to cache
					if (!TextUtils.equals(AylaLanMode.device.dsn, AylaLanMode.savedLanModeDevice.dsn)) {
						saveToLog("%s, %s, %s:%s, %s", "E", "AylaDevice", "AylaLanMode.device.dsn", "!= savedLanModeDevice.dsn", "lanModeDisable");
					} else {
						String jsonProperties = AylaSystemUtils.gson.toJson(AylaLanMode.device.properties, AylaProperty[].class);
						AylaCache.save(AML_CACHE_PROPERTY, AylaLanMode.device.dsn, jsonProperties);
					}
				} catch (Exception e) {
					AylaSystemUtils.saveToLog("%s %s %s:%s %s", "E", "AylaDevice", "saveCacheProperties", e.getCause(), "lanModeDisable");
					e.printStackTrace();
				}
				//TODO: let device know session has ended, TBD
			}  else {
				AylaSystemUtils.saveToLog("%s, %s, %s, %s", "I", "AylaDevice", "saveCacheProperties:null", "lanModeDisable");
			}
		}  else {
			AylaSystemUtils.saveToLog("%s, %s, %s, %s", "I", "AylaDevice", "lanModeState:DISABLED", "lanModeDisable");
		}
	}

	protected boolean isLanModeEnabled() {
		return (TextUtils.equals(this.lanModeConfig.status, "enable"));   
	}
	
	/**
	 * Start a new session or extend the existing one if it's already up
	 * called via the lan mode sessionTimer.
	 * 
	 * @return
	 */
	static String logMsg1 = "", logMsg2 = "";
	protected static AylaRestService startLanModeSession(
			AylaDevice device
			, int method
			, final boolean haveDataToSend) {
		return startLanModeSession (device, method, haveDataToSend, null);
	}
	protected static AylaRestService startLanModeSession(
			AylaDevice device
			, int method
			, final boolean haveDataToSend
			, final byte[] pubKey) {
		AylaSystemUtils.saveToLog("%s, %s, %s, %s", "I", "AylaDevice", "entry", "startLanModeSession");
		if (AylaLanMode.device == null) {
			if (device != null) {
				AylaLanMode.device = device;
			} else {
				lanModeSessionFailed(); // v3.00a
				saveToLog("%s, %s, %s%s, %s", "E", "AylaDevice", "device", "null", "startLanModeSession");
			}
		}

		if (AylaLanMode.lanModeState != lanMode.RUNNING) {
			lanModeSessionFailed(); // v3.00a
			saveToLog("%s, %s, %s:%s, %s", "W", "AylaDevice", "lanModeState", "!RUNNING", "localRegistration");
			return null;
		}

		if (AylaLanMode.device == null) { 	// device is not lan mode enabled
			lanModeSessionFailed(); // v3.00a
			saveToLog("%s, %s, %s:%s, %s", "E", "AylaDevice", "device", "null", "localRegistration");
			return null;
		}

		AylaReachability.determineDeviceReachability(true); // determine connectivity, reachabl1ity, and notify registered handlers, v3.00
		if (!AylaReachability.isDeviceLanModeAvailable() && pubKey == null) {
			lanModeSessionFailed(); // v3.00a
			return null;
		}

		if (!device.lanEnabled) { 	// device is not lan mode enabled
			lanModeSessionFailed(); // v3.00a
			AylaReachability.setDeviceReachability(AylaNetworks.AML_REACHABILITY_UNREACHABLE);
			return null;
		}

		String url = String.format("%s%s%s", lanIpServiceBaseURL(AylaLanMode.device.lanIp), "local_reg", ".json");
		AylaRestService rs = new AylaRestService(localRegistration, url, method);

		String msg1 = String.format("%s, %s, %s:%s, %s", "I", "AylaDevice", "url", url, "localRegistration");
		if (TextUtils.equals(msg1, logMsg1)) {
			consoleMsg(logMsg1, loggingLevel);
		} else {
			saveToLog("%s", msg1); logMsg1 = msg1;
		}

		// {"local_reg":{"ip":"192.168.0.2","port":10275,"uri":"/local_reg","notify":1}} for normal lanmode.
		// {"local_reg":{"ip":"192.168.0.2","port":10275,"uri":"/local_reg","notify":1, "key":"..."}} for secure wifi setup.
		String notifyString = null;
		if (haveDataToSend) {
			notifyString = "\"notify\":1";
		} else {
			notifyString = "\"notify\":0";
		}
		
		if (pubKey!= null) { // Override haveDataToSend for secure wifi setup. 
			notifyString = "\"notify\":1";
		}
		
		StringBuilder entity = new StringBuilder();
		
		entity.append("{\"local_reg\":{")
			.append("\"ip\":").append("\"").append(AylaLanMode.serverIpAddress).append("\",")
			.append("\"port\":").append(serverPortNumber).append(",")
			.append(notifyString).append(",")
			.append("\"uri\":").append("\"" + serverPath + "\"");
		
		if (pubKey != null) { // for secure wifi setup
			entity.append(",\"key\":\"")
				.append(AylaEncryptionHelper.encode(pubKey))
				.append("\"");
		}
		entity.append("}}");
		rs.setEntity(entity.toString());
//		String msg2 = String.format(Locale.getDefault(),"%s, %s, %s:%s, %s:%d, %s, %s:%s, %s", "I", "AylaDevice", "serverIpAddress", AylaLanMode.serverIpAddress,
//				"serverPortNumber", serverPortNumber, notifyString, "serverPath", serverPath, "localRegistration");
		String msg2 = String.format(Locale.getDefault(),"%s, %s, entity:%s, %s", "I", "AylaDevice", entity.toString(), "localRegistration");         
		//String msg = String.format("%s, %s, %s:%s, %s", "I", "AylaLanMode", "url", url, "localRegistration");

		if (TextUtils.equals(msg2, logMsg2)) {
			consoleMsg(logMsg2, loggingLevel);
		} else {
			saveToLog("%s", msg2); logMsg2 = msg2;
		}
		rs.execute(); // send the session request/extension
		return rs;
	}

	static String logMsg3 = null;
	private final static Handler localRegistration = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				if (msg.arg1 == 202) { //The Module will initiate a key exchange to establish or get commands to extend
					AylaReachability.setDeviceReachability(AML_REACHABILITY_REACHABLE);

					String msg3 = String.format(Locale.getDefault(),"%s, %s, %s:%d, %s", "I", "AylaDevice", "success", msg.arg1, "localRegistration_handler");
					if (TextUtils.equals(msg3, logMsg3)) {
						consoleMsg(logMsg3, loggingLevel);
					} else {
						saveToLog("%s", msg3); logMsg3 = msg3;
					}
				} else {
					AylaReachability.setDeviceReachability(AML_REACHABILITY_REACHABLE);
					saveToLog("%s, %s, %s:%d, %s, %s", "E", "AylaDevice", "UnknownStatus", msg.arg1, msg.obj, "localRegistration_handler");
				}
			} else {
				switch (msg.arg1) {
				case 400: //400: Forbidden - Bad Request (JSON parse failed) 
					AylaLanMode.sessionState = lanModeSession.DOWN;
					break;
				case 403: //403: Forbidden - lan_ip on a different network 
					AylaLanMode.sessionState = lanModeSession.DOWN;
					break;
				case 404: //404: Not Found - Lan Mode is not supported by this module
					AylaLanMode.sessionState = lanModeSession.DOWN;
					break;
				case 503: //503: Service Unavailable - Insufficient resources or maximum number of sessions exceeded
					AylaLanMode.sessionState = lanModeSession.DOWN;
					break;
				default:
					AylaLanMode.sessionState = lanModeSession.DOWN;
					break;
				}

				AylaReachability.setDeviceReachability(AML_REACHABILITY_UNREACHABLE);
				lanModeSessionFailed(); // v3.00

				saveToLog("%s, %s, %s:%d, %s, %s", "W", "AylaDevice", "error", msg.arg1, msg.obj, "localRegistration_handler");
			}
		}		
	};

	// Called after successful key exchange
	static String logMsg4 = null;
	protected static void lanModeSessionInit() {
		AylaLanMode.seq_no = 0;
		AylaLanMode.sessionState = lanModeSession.UP;
		AylaReachability.setDeviceReachability(AML_REACHABILITY_REACHABLE);
		String logMsg= String.format(Locale.getDefault(),"%s, %s, %s:%s, %s", "I", "AylaDevice", "sessionState", AylaLanMode.sessionState, "lanModeSessionInit");
		if (TextUtils.equals(logMsg, logMsg4)) {
			consoleMsg(logMsg4, loggingLevel);
		} else {
			saveToLog("%s", logMsg); logMsg4 = logMsg;
		}
		
		String response = "{\"type\":\""  + AML_NOTIFY_TYPE_SESSION + "\",\"dsn\":\"" + AylaLanMode.device.dsn + "\"}";
		
		AylaNotify.returnToMainActivity(null, response, 200, 0, false); // notify app to GET current values from device
//		AylaNotify.returnToMainActivity(null, response, 200, AylaRestService.LAN_MODE_SESSION_ESTABLISHED, false); // notify app to GET current values from device
	}

	protected static void lanModeSessionFailed() {
		AylaLanMode.sessionState = lanModeSession.DOWN;
		AylaReachability.setDeviceReachability(AML_REACHABILITY_UNREACHABLE);
		saveToLog("%s, %s, %s.", "D", "AylaDevice", "lanModeSessionFailed()");
		String response = null;
		if (AylaLanMode.device != null && AylaLanMode.device.dsn != null) {
			response = "{\"type\":\""  + AML_NOTIFY_TYPE_SESSION + "\",\"dsn\":\"" + AylaLanMode.device.dsn + "\"}";
		} else {
			response = "{\"type\":\""  + AML_NOTIFY_TYPE_SESSION + "\",\"dsn\":\"" + "unknown" + "\"}";
		}
		
		AylaNotify.returnToMainActivity(null, response, 404, 0, false); // notify app unable to establish lan mode
	}

	// Extend the Lan Mode session
	protected static void extendLanModeSession(int method, Boolean haveDataToSend) {
		if (AylaLanMode.device != null) { // session has not started yet
			startLanModeSession(AylaLanMode.device, method, haveDataToSend); // extend the session
		}
	}

	/**
	 * Properties full set: {Pa, Pb, Pc, Pd, Pe}
	 * 
	 * Firstly retrieve {Pa, Pb, Pc}, and so is memory/cache at this time.
	 * And some time later retrieve {Pc`, Pd`, Pe`} ,both memory/cache 
	 * should be updated to {Pa, Pb, Pc`, Pd`, Pe`}
	 * 
	 * @param newly retrieved properties
	 * */
	public void mergeNewProperties(AylaProperty[] newProperties) {
		if (newProperties == null || newProperties.length <1) {
			return;
		}
		
		Map<String, AylaProperty> tempPropertyMap = new HashMap<String, AylaProperty>();
		if (this.properties!= null) {
			for (AylaProperty p : this.properties) {
				tempPropertyMap.put(p.name(), p);
			}
		}
		
		for (AylaProperty p : newProperties) {
			tempPropertyMap.put(p.name(), p);
		}
		
		this.properties = tempPropertyMap.values().toArray(new AylaProperty[tempPropertyMap.size()]);
		String nps = AylaSystemUtils.gson.toJson(this.properties, AylaProperty[].class);
		AylaCache.save(AML_CACHE_PROPERTY, this.dsn, nps);
	}// end of mergeNewProperties   
	
	
	
	// New lan mode methods
	public boolean isLanModeActive() {
		if(AylaLanMode.sessionState == lanModeSession.UP &&
				AylaLanMode.device != null  &&
				TextUtils.equals(this.dsn, AylaLanMode.device.dsn) &&    
				AylaLanMode.device.properties != null) {
			return true;
		}
		return false;
	}

	protected Integer lanModeWillSendEntity(AylaLanCommandEntity entity) {
		return 200;
	}
	
	protected Integer updateProperty(String propertyName, String value) {
		property = this.findProperty(propertyName);
		int status = 200;
		if (property != null) {
			property.value = value;
			property.updateDatapointFromProperty();
			property.lanModeEnable();
		}
		else {
			AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", propertyName, "PropertyNotFound", "updateProperty");
			status = 404; // 404 Not Found
		}
		return status;
	}

	protected Integer lanModeUpdateProperty(String propertyName, String value, Boolean toBeNotified) {
		int propertyUpdateStatus = this.updateProperty(propertyName, value);
		switch (propertyUpdateStatus) {
        case 404:
            return propertyUpdateStatus;
        default:
            break;
		}
		
		if(toBeNotified) {
			AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "AylaLanMode", "statusFromDevice", "updateReceived", "lanModeUpdateProperty");
			String response = "{\"type\":\"" + AML_NOTIFY_TYPE_PROPERTY + "\",\"dsn\":\"" + this.dsn +
						  "\",\"names\":[\"" + propertyName + "\"]}";
			AylaNotify.returnToMainActivity(null, response, propertyUpdateStatus, 0, true); // request originated from device
		}
		return 200;
	}
	
	protected Integer lanModeUpdateProperty(String cmdIdStr, int status, String propertyName, String value, AylaRestService rs, Integer leftCount) {
		int propertyUpdateStatus = this.updateProperty(propertyName, value);
		int updateStatus = propertyUpdateStatus;
		
		if (rs != null) {
			
			if (rs.RequestType == AylaRestService.GET_PROPERTIES_LANMODE) {
				if(status < 200 || status > 299 || propertyName == null){
					//TODO: do we really need to clear send queue here?
					AylaLanMode.clearSendQueue();
					AylaLanMode.clearCommandsOutstanding();
					updateStatus = 400;
					leftCount = 0;
				}
				else {
					updateStatus = 200;
				}
				if (leftCount == 0) { // wait for all to return
					String jsonProperties = AylaSystemUtils.gson.toJson(this.properties,AylaProperty[].class);
					AylaProperty.returnToMainActivity(rs, jsonProperties, updateStatus, 0, false);
				}
			
				saveToLog("%s, %s, %s:%s, %s:%s, %s:%s, %s", "I", "AylaLanMode", "method", "lanModeUpdateProperty", "updateStatus", updateStatus, "count", leftCount, "Response_datapoint");
			} else if (rs.RequestType == AylaRestService.GET_DATAPOINT_LANMODE) {
				saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "AylaLanMode", "method", "GET_DATAPOINT_LANMODE_", "updateStatus", updateStatus, "Response_datapoint");
				if(updateStatus < 300) { //TODO: Now do not bother app to handle all failure updates. We can discuss 
					String jsonDatapoint = "[" + AylaSystemUtils.gson.toJson(property.datapoint,AylaDatapoint.class) + "]";
					AylaDatapoint.returnToMainActivity(rs, jsonDatapoint, updateStatus, 0);
				}
			} else {
				saveToLog("%s, %s, %s:%d, %s", "E", "AylaLanMode", "requestType_NotFound", rs.RequestType, "Response_datapoint");
				updateStatus = 404;
			}
		} else {
			saveToLog("%s, %s, %s:%s, %s", "E", "AylaLanMode", "RestService", "NotFound", "Response_datapoint"); 
			updateStatus = 404;
		}

		return updateStatus;
	}	
	
	/**
	 * Return property value on Android side. return whole command string on iOS side
	 * */ 
	protected String lanModeToDeviceUpdate(AylaRestService rs, AylaProperty property, String value, int cmdId) {
		return value;
	}
	
	//TODO: replace implementation with StringBuilder. 
	protected String zCmdToLanModeDevice(String type, String propertyName, String data, String uri, int cmdId) {
		String jsonCommand = "";
		if (AylaLanMode.lanModeState != lanMode.DISABLED) {
			// build property for lan mode device
			jsonCommand = jsonCommand + "{\"cmds\":[";
			jsonCommand = jsonCommand + "{\"cmd\":";
			jsonCommand = jsonCommand + "{\"cmd_id\":" + cmdId + ",";
			jsonCommand = jsonCommand + "\"method\":" + "\""+ type +"\"" + ",";
			jsonCommand = jsonCommand + "\"resource\":" + "\"" + "property.json?name=" + propertyName + "\"" + ",";
			jsonCommand = jsonCommand + "\"data\":" + "\"" + data + "\"" + ",";
			jsonCommand = jsonCommand + "\"uri\":" + "\"" + uri + "\"";
			jsonCommand = jsonCommand + "}}";
			jsonCommand = jsonCommand + "]}";
		}
		return jsonCommand;
	}
	
	
	protected String lanModeToDeviceCmd(AylaRestService rs, String type, String uri, Object obj) {
		String cmd = "";
		if (TextUtils.equals(type, "GET")) {
			if (TextUtils.equals(uri, "datapoint.json")) {
				//GET property/datapoint.json
				AylaProperty property = (AylaProperty)obj;
				if(property.datapoint == null) {
					property.datapoint = new AylaDatapoint();
				}
				cmd = property.datapoint.getFromLanModeDevice(rs, property);
			}
		}
		return cmd;
	}

	protected AylaDevice lanModeEdptFromDsn(String dsn) {
		return TextUtils.equals(dsn, this.dsn)? this: null;
	}
	
	protected String lanModePropertyNameFromEdptPropertyName(String name) {
		return name;
	}
	
	protected void didEnableLanMode()
	{
	    //Nothing to be set in AylaDevice
	}

	protected void didDisableLanMode()
	{
	    //Nothing to be set in AylaDevice
	}
	
	// Return the device property matching the product name
	public AylaProperty findProperty(String propertyName) {
		if (this.properties != null) {
			for (AylaProperty property: this.properties) {
				if (TextUtils.equals(property.name, propertyName)) {
					return property;
				}
			}
		}
		return null;
	}

	private void updateDevicesCacheLanIp(String discoveredLanIp) {
		String savedJsonDeviceContainers;
		savedJsonDeviceContainers =  AylaCache.get(AML_CACHE_DEVICE);
		if (!TextUtils.isEmpty(savedJsonDeviceContainers)) {
			String newJsonDeviceContainers = savedJsonDeviceContainers.replace(this.lanIp, discoveredLanIp);
			AylaCache.save(AML_CACHE_DEVICE, newJsonDeviceContainers);
			AylaSystemUtils.saveToLog("%s %s %s:%s %s", "I", "Devices", "discoveredLanIp" , discoveredLanIp, "updateDevicesCacheLanIp");
		}
	}

	// Return the device schedule matching the product name
	public AylaSchedule findSchedule(String scheduleName) {
		if (this.schedules != null) {
			for (AylaSchedule schedule: this.schedules) {
				if ( TextUtils.equals(schedule.name, scheduleName) ) {
					return schedule;
				}
			}
		}
		return null;
	}
}







