//
//  AylaUserPlugin.h
//  Connected-Solution
//
//

#import <Cordova/CDVPlugin.h>

@interface AylaUserPlugin : CDVPlugin

    - (void) login:(CDVInvokedUrlCommand *)command;
    - (void) logout:(CDVInvokedUrlCommand *)command;
    - (void) refreshAccessToken:(CDVInvokedUrlCommand *)command;

    - (void) getDevices:(CDVInvokedUrlCommand *)command;
    - (void) getProperties:(CDVInvokedUrlCommand *)command;
    - (void) createDatapoint:(CDVInvokedUrlCommand *)command;
    - (void) getDatapointsByActivity:(CDVInvokedUrlCommand *)command;

@end
