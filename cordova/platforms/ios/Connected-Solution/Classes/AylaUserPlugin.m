//
//  AylaUserPlugin.m
//  Connected-Solution
//

#import "AylaUserPlugin.h"
#import <Cordova/CDVPlugin.h>
#import <Crashlytics/Crashlytics.h>
#import "AylaUser.h"
#import "AylaResponse.h"
#import "AylaError.h"
#import "AylaDevice.h"
#import "AylaDeviceNode.h"
#import "AylaDeviceSupport.h"

//TESTING
#import "AylaDeviceGateway.h"

@interface AylaUserPlugin ()

@property (nonatomic, strong) NSArray *devices;

- (void) sendDeviceProperites:(NSNumber*) deviceId devices:(NSArray *) devices callbackId:(NSString*) callbackId;
- (NSDictionary*) parseDatapointIntoDictionary:(AylaDatapoint*)datapoint;
    
- (void) sendAylaOkPluginResult:(NSString*)callbackId withDictionary:(NSDictionary *) jsonObj;
- (void) sendAylaOkPluginResult:(NSString*)callbackId withArray:(NSArray *) jsonArray;
- (void) sendAylaErrorPluginResult:(AylaError*) err callbackId:(NSString*)callbackId;
@end

@implementation AylaUserPlugin

- (void) pluginInitialize {
    CLS_LOG(@"AylaUserPlugin -> pluginInitialized");

//    TODO:
//     1) Eventually need a way to configure web service endpoint urls for production.
//     2) Load Api Id and Secret keys from with mobile app code so its not exposed in the javascript/html.
}

/**
     The method runs when the UIWebView navigates to a new page or refreshes, which reloads the JavaScript.
 */
- (void) onReset {
//  TODO: Determine if we need to handle cleanup of any long running processes
    CLS_LOG(@"AylaUserPlugin -> onReset");
}

/**
 Login method.
 */
- (void) login:(CDVInvokedUrlCommand *)command {
    
    NSString *userName = [command.arguments objectAtIndex:0];
    NSString *password = [command.arguments objectAtIndex:1];
    NSString *appId = [command.arguments objectAtIndex:2];
    NSString *appSecret = [command.arguments objectAtIndex:3];
    
    [self.commandDelegate runInBackground:^{
        [AylaUser login:userName password:password appId:appId appSecret:appSecret success:^(AylaResponse *response, AylaUser *user) {
            NSString* accessToken = user.accessToken;
            NSString* expiresIn = [NSString stringWithFormat:@"%lu", (unsigned long)user.expiresIn];
            NSString* refreshToken = user.refreshToken;
            
            CLS_LOG(@"AylaUserPlugin -> Login success");
            
            NSDictionary *jsonObj = [ [NSDictionary alloc]
                                     initWithObjectsAndKeys :
                                     @"true", @"success",
                                     accessToken, @"access_token",
                                     expiresIn, @"expires_in",
                                     refreshToken, @"refresh_token",
                                     nil
                                     ];

    //        Sample web service response json. Trying to match this above, minus roles.
    //        {
    //            "access_token": "cd36da4ab6e64a158387517cbc96c8db",
    //            "expires_in": 86400,
    //            "refresh_token": "5810f11e11354c92821e7cd1a6a8a8fe",
    //            "role": "EndUser",
    //            "role_tags": []
    //        }

            self.devices = NULL;

            [self sendAylaOkPluginResult: command.callbackId withDictionary:jsonObj];
        } failure:^(AylaError *err) {
            NSString* httpStatusCode = [NSString stringWithFormat: @"%ld", (long)[err httpStatusCode]];
            CLS_LOG(@"Login failed: status:%@", httpStatusCode);

            [self sendAylaErrorPluginResult:err callbackId:command.callbackId];
        }];
    }];
    
}

- (void) logout:(CDVInvokedUrlCommand *)command {
    
    NSDictionary* params = [ [NSDictionary alloc]
                            initWithObjectsAndKeys :
                            @(YES), kAylaUserLogoutClearCache,
                            nil
                            ];
    [self.commandDelegate runInBackground:^{
        [AylaUser logoutWithParams:params success:^(AylaResponse *response) {
            self.devices = NULL;
            
            CDVPluginResult *pluginResult = [CDVPluginResult
                                             resultWithStatus    : CDVCommandStatus_OK
                                             messageAsDictionary : @{}
                                             ];
            
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            
        } failure:^(AylaError *err) {
            [self sendAylaErrorPluginResult:err callbackId:command.callbackId];
        }];
    }];
}

- (void) refreshAccessToken:(CDVInvokedUrlCommand *)command {
    NSString *refreshToken = [command.arguments objectAtIndex:0];
    
    [self.commandDelegate runInBackground:^{
        [AylaUser refreshAccessTokenWithRefreshToken:refreshToken success:^(AylaResponse *response, AylaUser *refreshed) {
            NSString* accessToken = refreshed.accessToken;
            NSString* expiresIn = [NSString stringWithFormat:@"%lu", (unsigned long)refreshed.expiresIn];
            NSString* refreshToken = refreshed.refreshToken;

            NSDictionary *jsonObj = [ [NSDictionary alloc]
                                     initWithObjectsAndKeys :
                                     @"true", @"success",
                                     accessToken, @"access_token",
                                     expiresIn, @"expires_in",
                                     refreshToken, @"refresh_token",
                                     nil
                                     ];
            
            [self sendAylaOkPluginResult: command.callbackId withDictionary:jsonObj];
            
        } failure:^(AylaError *err) {
            [self sendAylaErrorPluginResult:err callbackId:command.callbackId];
        }];
    }];
}

/**
  Matching the following web server json.
 {
     "product_name": "My Gateway 1",
     "model": "TEST",
     "dsn": "VXSA00000000003",
     "oem_model": "OTEST",
     "template_id": null,
     "mac": "abcd12345603",
     "lan_ip": null,
     "connected_at": null,
     "key": 55219,
     "lan_enabled": false,
     "has_properties": true,
     "product_class": "",
     "connection_status": null,
     "lat": null,
     "lng": null,
     "device_type": "Wifi"
     "gateway_dsn" : "AC000W000432578"
 };
 */
- (void) getDevices:(CDVInvokedUrlCommand *)command {
    [self.commandDelegate runInBackground:^{
        [AylaDevice getDevices:NULL success:^(AylaResponse *response, NSArray *devices) {
            //Checking all Devices to see if the device is also a AylaDeviceNode
             for (AylaDevice *device in devices) {
                 NSLog(@"DSN: %@ deviceType: %@  isAylaDeviceNode: %@ isAylaDeviceGateway: %@",
                       device.dsn, device.deviceType,
                       [device isKindOfClass:[AylaDeviceNode class]] ? @"yes" : @"no",
                       [device isKindOfClass:[AylaDeviceGateway class]] ? @"yes" : @"no"
                       );
             }
            
            
            self.devices = devices;
            
            NSMutableArray *deviceArray = @[].mutableCopy;
            
            for (AylaDevice *device in devices){
                //NOTE: CamelCase prop names are not yet part of the the webservice json and could be removed from this list if needed.
                NSDictionary * deviceDictionary =  @{
                                                    @"key": device.key ? device.key : [NSNull null],
                                                    @"product_name": device.productName ? device.productName : [NSNull null],
                                                    @"model": device.model ? device.model : [NSNull null],
                                                    @"dsn": device.dsn ? device.dsn : [NSNull null],
                                                    @"oem_model": device.oemModel ? device.oemModel : [NSNull null],
                                                    @"device_type": device.deviceType ? device.deviceType : [NSNull null],
                                                    @"connected_at": device.connectedAt ? device.connectedAt : [NSNull null],
                                                    @"mac": device.mac ? device.mac : [NSNull null],
                                                    @"lan_ip": device.lanIp ? device.lanIp : [NSNull null],
                                                    @"swVersion": device.swVersion ? device.swVersion : [NSNull null],
                                                    @"ssid": device.ssid ? device.ssid : [NSNull null],
                                                    @"product_class": device.productClass ? device.productClass : [NSNull null],
                                                    @"has_properties": device.hasProperties ? device.hasProperties : [NSNull null],
                                                    @"ip": device.ip ? device.ip : [NSNull null],
                                                    @"lan_enabled": device.lanEnabled ? device.lanEnabled : [NSNull null],
                                                    @"connection_status": device.connectionStatus ? device.connectionStatus : [NSNull null],
                                                    @"templateId": device.templateId ? device.templateId : [NSNull null],
                                                    @"lat": device.lat ? device.lat : [NSNull null],
                                                    @"lng": device.lng ? device.lng : [NSNull null],
                                                    @"userId": device.userId ? device.userId : [NSNull null],
                                                    @"moduleUpdatedAt": device.moduleUpdatedAt ? device.moduleUpdatedAt : [NSNull null],
                                                    @"registrationType": device.registrationType ? device.registrationType : [NSNull null],
                                                    @"registrationToken": device.registrationToken ? device.registrationToken : [NSNull null],
                                                    @"setupToken": device.setupToken ? device.registrationToken : [NSNull null],
                                                   //device.retrievedAt, @"retrievedAt", //TODO covert date to string.
                                                   };
                
                
                
                if ([device isKindOfClass:[AylaDeviceNode class]]) {
                    
                    NSMutableDictionary *nodeDeictionary = deviceDictionary.mutableCopy;
                    AylaDeviceNode *node = (AylaDeviceNode*)device;
                    NSObject* dsn = node.gatewayDsn ? node.gatewayDsn : [NSNull null];
                    [nodeDeictionary setObject:dsn forKey:@"gateway_dsn"];
                    
                    deviceDictionary = nodeDeictionary.copy;
                }
                
                [deviceArray addObject:deviceDictionary];
            }

            CDVPluginResult *pluginResult = [CDVPluginResult
                                             resultWithStatus    : CDVCommandStatus_OK
                                             messageAsArray: deviceArray
                                             ];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(AylaError *err) {
            [self sendAylaErrorPluginResult:err callbackId:command.callbackId];
        }];
    }];
}

- (void) getProperties:(CDVInvokedUrlCommand *)command {
    NSNumber *deviceId = [command.arguments objectAtIndex:0];
    
    [self.commandDelegate runInBackground:^{
        if(self.devices) {
            [self sendDeviceProperites:deviceId devices:self.devices callbackId:command.callbackId];
        } else {
            [AylaDevice getDevices:NULL success:^(AylaResponse *response, NSArray *devices) {
                self.devices = devices;
                
                [self sendDeviceProperites:deviceId devices:self.devices callbackId:command.callbackId];
                
            } failure:^(AylaError *err) {
                [self sendAylaErrorPluginResult:err callbackId:command.callbackId];
            }];
        }
    }];
}

/**
 * Note: This method creates a temporary AylaProperty, Therefor the return property value will be set back on the temporary AylaProperty and then abandoned.
 */
- (void) createDatapoint:(CDVInvokedUrlCommand *)command {
    
    NSNumber *deviceId = [command.arguments objectAtIndex:0];
    NSNumber *propId = [command.arguments objectAtIndex:1];
    NSString *baseType = [command.arguments objectAtIndex:2];
    
    [self.commandDelegate runInBackground:^{
        AylaProperty * property = [[AylaProperty alloc] initDevicePropertiesWithDictionary:@{ @"device_key": deviceId, @"key":propId, @"base_type": baseType}];
     
        AylaDatapoint *datapoint = [[AylaDatapoint alloc] init];
       
        id something = [command argumentAtIndex:3];
      
        if ([something isKindOfClass:[NSString class]]) {
            datapoint.sValue =[command.arguments objectAtIndex:3];
        } else {
            NSNumber *aThing = [command.arguments objectAtIndex:3];
            datapoint.nValue = [NSNumber numberWithInt:aThing.intValue];
        }
        
        [AylaDatapoint createDatapoint:property datapoint:datapoint success:^(AylaResponse *response, AylaDatapoint *datapointCreated) {

            NSDictionary *jsonObj = [self parseDatapointIntoDictionary: datapointCreated];
            
            NSDictionary *datapointDictionary = @{ @"datapoint": jsonObj };
            
            [self sendAylaOkPluginResult:command.callbackId withDictionary: datapointDictionary];
        } failure:^(AylaError *err) {
            [self sendAylaErrorPluginResult:err callbackId:command.callbackId];
        }];
    }];
}

//NOTE:TODO Still under dev.
- (void) getDatapointsByActivity:(CDVInvokedUrlCommand *)command {
    NSNumber *deviceId = [command.arguments objectAtIndex:0];
    NSNumber *propId = [command.arguments objectAtIndex:1];
    NSString *jsonObjectString = [command.arguments objectAtIndex:2];
    
    
    NSError *jsonError;
    NSData *objectData = [jsonObjectString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonParams = [NSJSONSerialization JSONObjectWithData:objectData
                                                         options:NSJSONReadingMutableContainers
                                                           error:&jsonError];
    
    [self.commandDelegate runInBackground:^{
        AylaProperty * property = [[AylaProperty alloc] initDevicePropertiesWithDictionary:@{ @"device_key": deviceId, @"key":propId }];
        
        [AylaDatapoint getDatapointsByActivity:property callParams:jsonParams success:^(AylaResponse *response, NSArray *datapoints) {
            NSMutableArray *returnAray = @[].mutableCopy;
            
            for (AylaDatapoint *datapoint in datapoints){
                [returnAray addObject: [self parseDatapointIntoDictionary: datapoint]];
            }
            
            [self sendAylaOkPluginResult:command.callbackId withArray:returnAray];
        } failure:^(AylaError *err) {
            [self sendAylaErrorPluginResult:err callbackId:command.callbackId];
        }];
    }];
}

- (NSDictionary*) parseDatapointIntoDictionary:(AylaDatapoint*)datapoint {
    NSString* value = datapoint.value;
	NSNumber *numValue;
	bool isNumeric = NO;
    
    if (!value) {
        value = datapoint.sValue;
    }
    
    if (!value) {
		isNumeric = YES;
		numValue = datapoint.nValue;
    }

    NSDictionary * dictionary =  @{
								   @"value": isNumeric ? numValue : (value ? value : [NSNull null]),
                                   @"created_at": datapoint.createdAt ? datapoint.createdAt : [NSNull null],
                                   @"updated_at": datapoint.createdAt ? datapoint.createdAt : [NSNull null],
                                   @"retrieved_at": datapoint.retrievedAt ? datapoint.retrievedAt : [NSNull null]
                                   };
    
    return dictionary;
}

- (void) sendAylaErrorPluginResult:(AylaError*) err callbackId:(NSString*)callbackId {
    NSString* httpStatusCode = [NSString stringWithFormat: @"%ld", (long)[err httpStatusCode]];
    NSString* errorCode = [NSString stringWithFormat: @"%ld", (long)err.errorCode];
    
    NSDictionary *jsonObj = [ [NSDictionary alloc]
                             initWithObjectsAndKeys :
                             httpStatusCode, @"httpStatusCode",
                             errorCode, @"errorCode",
                             nil
                             ];
    
    CDVPluginResult *pluginResult = [ CDVPluginResult
                                     resultWithStatus    : CDVCommandStatus_ERROR
                                     messageAsDictionary : jsonObj
                                     ];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
}


- (void) sendAylaOkPluginResult:(NSString*)callbackId withDictionary:(NSDictionary *) jsonObj{
    
    CDVPluginResult *pluginResult = [CDVPluginResult
                                     resultWithStatus    : CDVCommandStatus_OK
                                     messageAsDictionary : jsonObj
                                     ];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
}

- (void) sendAylaOkPluginResult:(NSString*)callbackId withArray:(NSArray *) jsonArray {

    CDVPluginResult *pluginResult = [CDVPluginResult
                                     resultWithStatus    : CDVCommandStatus_OK
                                     messageAsArray: jsonArray
                                     ];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
}


- (void) sendDeviceProperites:(NSNumber*) deviceId devices:(NSArray *) devices callbackId:(NSString*) callbackId {
    AylaDevice* device;
    
    for (AylaDevice* aDevice in devices){
        if ([aDevice.key isEqualToNumber: deviceId]){
            device = aDevice;
            break;
        }
    }
    
    if (device) {
        [device getProperties:nil success:^(AylaResponse *response, NSArray *properties) {
            
            NSMutableArray *returnArray = @[].mutableCopy;
			bool isNumeric = NO;
			NSNumber *numberValue;
            
            for (AylaProperty *prop in properties){
				
				// if the base type is a string then we dont check.
				if (![prop.baseType isEqualToString:@"string"] && prop.value) {
					NSScanner *scanner = [NSScanner scannerWithString:prop.value];
					isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
					
					if (isNumeric) {
						numberValue = [NSNumber numberWithInteger: [prop.value intValue]];
					}
				}
				
                NSDictionary * propertyDictionary = @{
                                                   @"key": prop.key,
                                                   @"base_type": prop.baseType ? prop.baseType :[NSNull null],
                                                   @"name": prop.name ? prop.name :[NSNull null],
                                                   @"direction": prop.direction ? prop.direction :[NSNull null],
                                                   @"retrieved_at": prop.retrievedAt ? prop.retrievedAt :[NSNull null],
												   @"value": isNumeric ? numberValue : (prop.value ? prop.value  :[NSNull null]),
                                                   @"data_updated_at": prop.dataUpdatedAt ? prop.dataUpdatedAt :[NSNull null],
                                                   @"display_name": prop.displayName ? prop.displayName :[NSNull null],
                                                   @"owner": prop.owner ? prop.owner :[NSNull null],
                                                   };
                                                   
                [returnArray addObject:@{@"property": propertyDictionary}];
            }
            
            CDVPluginResult *pluginResult = [CDVPluginResult
                                             resultWithStatus    : CDVCommandStatus_OK
                                             messageAsArray: returnArray
                                             ];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
        } failure:^(AylaError *err) {
            [self sendAylaErrorPluginResult:err callbackId:callbackId];
        }];
    } else {
        NSDictionary *jsonObj = [ [NSDictionary alloc]
                                 initWithObjectsAndKeys :
                                 @404, @"httpStatusCode",
                                 nil
                                 ];
        
        CDVPluginResult *pluginResult = [ CDVPluginResult
                                         resultWithStatus    : CDVCommandStatus_ERROR
                                         messageAsDictionary : jsonObj
                                         ];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    }
}

@end
