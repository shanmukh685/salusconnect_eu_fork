//
//  AylaLanModeSupport.h
//  Ayla Mobile Library
//
//  Created by Yipei Wang on 3/20/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AylaLanCommandEntity.h"
#import "AylaTimer.h"
@interface AylaLanMode(Support)

//---------helpful functions--------------
+ (AylaDevice *)          device;
+ (void)                  setDevice:(AylaDevice *)_device;
+ (NSMutableDictionary *) devices;
+ (int)                   sessionState;
+ (void)                  setSessionState:(enum lanModeSession) _sessionState;
+ (AylaTimer*)            sessionTimer;
+ (void)                  setSessionTimer: (AylaTimer*) _timer;
+ (BOOL)                  inSetupMode;
+ (void)                  inSetupMode:(BOOL)_inSetupMode;



// Handle command sequence number
+ (int)  seqNo;
+ (void) addSeqNo;

+ (AylaLanCommandEntity *)   nextInQueue;
+ (void)                     enQueue:(int)cmdId baseType:(int)baseType jsonString:(NSString *)jsonString;
+ (void)                     deQueue;
+ (void)                     clearQueue;
+ (int)                      commandsQueueCount;

// Handle command outstanding id
+ (int)                      nextCommandOutstandingId;
+ (void (^)(NSDictionary *)) getCommandsOutstanding:(NSString *)cmdId;
+ (void)                     removeCommandsOutstanding:(int)cmdId;
+ (void)                     clearCommandOutstanding;


+ (void)       sendNotifyToLanModeDevice:(int)cmdId baseType:(int)baseType
                              jsonString:(NSString *)jsonString
                                   block:(void(^)(NSDictionary *)) block;
+ (NSString *) buildToDeviceCommand:
                          (NSString *)method cmdId:(int)cmdId
                           resourse:(NSString *)resource data:(NSString *)data
                                uri:(NSString *) uri;

+ (void)       cleanCurrentEncSession;
@end