//
//  AylaEncryption.m
//  Ayla Mobile Library
//
//  Created by Yipei Wang on 2/13/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import "AylaEncryption.h"
#import "AylaNetworks.h"
#import "AylaDeviceSupport.h"
#import "AylaLanModeSupport.h"
#import <CommonCrypto/CommonHMAC.h>
#import <CommonCrypto/CommonCryptor.h>
#import "NSData+Base64.h"

@implementation AylaEncryption

static NSNumber *version = nil;
static NSNumber *proto1 = nil;
static NSNumber *keyId1 = nil;
//static NSString *createdAt = nil;
static int sessionId = -1;
static NSString *sRnd1 = nil;
static NSString *sRnd2 = nil;
static NSNumber *nTime1 =nil;
static NSNumber *nTime2 = nil;

static NSData *bLanKey = nil;
static NSString *sLanKey = nil;

static NSData *appSignKey = nil;
static NSData *appCryptoKey = nil;
static NSData *appIvSeed = nil;
static NSData *devSignKey = nil;
static NSData *devCryptoKey = nil;
static NSData *devIvSeed = nil;

static CCCryptorRef eCipher;
static CCCryptorRef dCipher;

+ (NSNumber *)version
{
    return version;
}
+ (void)setVersion:(NSNumber *)_version
{
    version = _version;
}
+ (NSNumber *)proto1
{
    return proto1;
}
+ (void)setProto1:(NSNumber *)_proto
{
    proto1 = _proto;
}
+ (NSNumber *)keyId1
{
    return keyId1;
}
+ (void)setKeyId1:(NSNumber *)_keyId
{
    keyId1 = _keyId;
}

+ (NSData*)devSignKey
{
    return devSignKey;
}
+ (NSData*)appSignKey
{
    return appSignKey;
}


// sRnd1 nTime1 from device
// sRnd2 nTime2 from local ios 
+ (int)generateSessionkeys:(NSDictionary *)params sRnd1:(NSString*)_sRnd1 nTime1:(NSNumber *)_nTime1 sRnd2:(NSString*)_sRnd2 nTime2:(NSNumber*)_nTime2
{    
    saveToLog(@"%@, %@, %@, %@", @"I", @"AylaEncryption", @"entry", @"generateSessionKeys");

    //get time and random number from server
    sRnd1 = _sRnd1;
    nTime1 = _nTime1;
    
    //get ios time and random number
    //NSNumber *curTime = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]*1000000];
    //createdAt = [NSString stringWithFormat:@"%@",curTime];
    sRnd2 = _sRnd2;
    nTime2 = _nTime2;
    
    NSData *bRnd1 = [sRnd1 dataUsingEncoding:NSUTF8StringEncoding];
    NSData *bRnd2 = [sRnd2 dataUsingEncoding:NSUTF8StringEncoding];
    
    
    if(params) {
        NSString *type = [params objectForKey:@"type"];
        if(type && [type isEqualToString:@"wifi_setup_dh"]) {
            bLanKey = [params objectForKey:@"data"];
        }
        else if(type && [type isEqualToString:@"wifi_setup_rsa"]) {
            bLanKey = [params objectForKey:@"data"];
        }
        else {
            return AML_ERROR_FAIL;
        }
    }
    else if([AylaLanMode device]!=nil && [[AylaLanMode device] isLanModeEnabled]){
        sLanKey = [[[AylaLanMode device] lanModeConfig] lanipKey];
        bLanKey = [sLanKey dataUsingEncoding:NSUTF8StringEncoding];
    }
    else{
        return AML_ERROR_FAIL;
    }

    NSString *sTime1 = [NSString stringWithFormat:@"%@", nTime1];
    NSString *sTime2 = [NSString stringWithFormat:@"%@", nTime2];
    
    NSData *bTime1 = [sTime1 dataUsingEncoding:NSUTF8StringEncoding];
    NSData *bTime2 = [sTime2 dataUsingEncoding:NSUTF8StringEncoding];
    
    
    // App Signing key:    <random_1> + <random_2> + <time_1> + <time_2> + 0
    // App Encrypting key: <random_1> + <random_2> + <time_1> + <time_2> + 1
    // App IV CBC seed:    <random_1> + <random_2> + <time_1> + <time_2> + 2
    
    Byte lastByte = 48;
    
    NSMutableData *bTempSeed = [[NSMutableData alloc] init];
    
    [bTempSeed appendBytes:bRnd1.bytes length:[bRnd1 length]];
    [bTempSeed appendBytes:bRnd2.bytes length:[bRnd2 length]];
    [bTempSeed appendBytes:bTime1.bytes length:[bTime1 length]];
    [bTempSeed appendBytes:bTime2.bytes length:[bTime2 length]];
    [bTempSeed appendBytes:&lastByte length:1];
    
    size_t seedLen = [bTempSeed length];
    
    NSData *level1 = [AylaEncryption hmacForKeyAndData:bLanKey  data:bTempSeed];
    NSMutableData *tmpSeed = [[NSMutableData alloc] initWithData:level1];
    [tmpSeed appendData:bTempSeed];
    appSignKey = [AylaEncryption hmacForKeyAndData:bLanKey data:tmpSeed];
    
    
    //App Encrypting Key
    lastByte++;
    [bTempSeed replaceBytesInRange:NSMakeRange(seedLen-1, 1) withBytes:&lastByte];
    level1 = [AylaEncryption hmacForKeyAndData:bLanKey data:bTempSeed];    
    [tmpSeed setLength:0]; [tmpSeed appendData:level1]; [tmpSeed appendData:bTempSeed];
    appCryptoKey = [AylaEncryption hmacForKeyAndData:bLanKey data:tmpSeed];
    
    //App IV CBC seed
    lastByte++;
    [bTempSeed replaceBytesInRange:NSMakeRange(seedLen-1, 1) withBytes:&lastByte];
    level1 = [AylaEncryption hmacForKeyAndData:bLanKey data:bTempSeed];
    [tmpSeed setLength:0]; [tmpSeed appendData:level1]; [tmpSeed appendData:bTempSeed];
    appIvSeed = [AylaEncryption hmacForKeyAndData:bLanKey data:tmpSeed];
    appIvSeed = [appIvSeed subdataWithRange:NSMakeRange(0, 16)];
    
    // NSLog(@"appSignKey: %@\n", [appSignKey description]);
    // NSLog(@"appCryptoKey: %@\n", [appCryptoKey description]);
    // NSLog(@"appIvSeed: %@\n", [appIvSeed description]);

    
    // Device Signing key:    <random_2> + <random_1> + <time_2> + <time_1> + 0
    // Device Encrypting key: <random_2> + <random_1> + <time_2> + <time_1> + 1
    // Device IV CBC seed:    <random_2> + <random_1> + <time_2> + <time_1> + 2
    
    lastByte = 48;

    [bTempSeed setLength:0];
    [bTempSeed appendBytes:bRnd2.bytes length:[bRnd2 length]];
    [bTempSeed appendBytes:bRnd1.bytes length:[bRnd1 length]];
    [bTempSeed appendBytes:bTime2.bytes length:[bTime2 length]];   
    [bTempSeed appendBytes:bTime1.bytes length:[bTime1 length]];
    [bTempSeed appendBytes:&lastByte length:1];
    
    seedLen = [bTempSeed length];
    level1 = [AylaEncryption hmacForKeyAndData:bLanKey data:bTempSeed];
    [tmpSeed setLength:0]; [tmpSeed appendData:level1]; [tmpSeed appendData:bTempSeed];
    devSignKey = [AylaEncryption hmacForKeyAndData:bLanKey data:tmpSeed];
    
    //App Encrypting Key
    lastByte++;
    [bTempSeed replaceBytesInRange:NSMakeRange(seedLen-1, 1) withBytes:&lastByte];
    level1 = [AylaEncryption hmacForKeyAndData:bLanKey data:bTempSeed];
    [tmpSeed setLength:0]; [tmpSeed appendData:level1]; [tmpSeed appendData:bTempSeed];
    devCryptoKey = [AylaEncryption hmacForKeyAndData:bLanKey data:tmpSeed];
    
    //App IV CBC seed
    lastByte++;
    [bTempSeed replaceBytesInRange:NSMakeRange(seedLen-1, 1) withBytes:&lastByte];
    level1 = [AylaEncryption hmacForKeyAndData:bLanKey data:bTempSeed];
    [tmpSeed setLength:0]; [tmpSeed appendData:level1]; [tmpSeed appendData:bTempSeed];
    devIvSeed = [AylaEncryption hmacForKeyAndData:bLanKey data:tmpSeed];
    devIvSeed = [devIvSeed subdataWithRange:NSMakeRange(0, 16)];
    
    // NSLog(@"devSignKey: %@\n", [devSignKey description]);
    // NSLog(@"devCryptoKey: %@\n", [devCryptoKey description]);
    // NSLog(@"devIvSeed: %@\n", [devIvSeed description]);
    
    sessionId ++;
    [AylaEncryption cipherClean:&eCipher];
    [AylaEncryption cipherClean:&dCipher];
    [AylaEncryption cipherInit:&eCipher operation:kCCEncrypt key:appCryptoKey iv:appIvSeed];
    [AylaEncryption cipherInit:&dCipher operation:kCCDecrypt key:devCryptoKey iv:devIvSeed];
      
    return AML_ERROR_OK;
}

+ (NSString *)encryptEncapsulateSign:(int)seqNo jsonString:(NSString *)jsonString baseType:(int) baseType sign:(NSData *)sign
{
    NSMutableString *jsonEnc = [NSMutableString stringWithFormat:@"{\"seq_no\":%d", seqNo];
    
    if(baseType == AYLA_LAN_PROPERTY){
    
        NSString *jsonProperty = jsonString;
        [jsonEnc appendFormat:@",\"data\":{\"properties\":["];
        if(jsonProperty!=nil){
            [jsonEnc appendString:jsonProperty];
        }
        [jsonEnc appendFormat:@"]}}"];
    }
    else if(baseType == AYLA_LAN_COMMAND){
        NSString *jsonCommand = jsonString;
        [jsonEnc appendFormat:@",\"data\":{\"cmds\":["];
        if(jsonCommand!=nil){
            [jsonEnc appendString:jsonCommand];
        }
        [jsonEnc appendFormat:@"]}}"];
    }
    else{
        [jsonEnc appendFormat:@",\"data\":%@",jsonString];
        [jsonEnc appendFormat:@"}"];
    }
    
    //  saveToLog(@"%@, %@, %@: %@, %@", @"I", @"AylaEncryption", @"enc", jsonEnc, @"encryptEncapsulateSign");
    NSData *signData = [AylaEncryption hmacForKeyAndData:sign data:[jsonEnc dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *encodedSign = [signData base64EncodedString];
       
    NSData *encryptedData  = [AylaEncryption cipherEncrypt:&eCipher plainText:jsonEnc];
    NSString *encodedEnc = [encryptedData base64EncodedString];
    
    NSString *send = [NSString stringWithFormat:@"{\"enc\":\"%@\",\"sign\":\"%@\"}", encodedEnc, encodedSign];
    
    //  saveToLog(@"%@, %@, %@: %@, %@", @"I", @"AylaEncryption", @"sendString", send, @"encryptEncapsulateSign");
    return send;
}




+ (BOOL)cipherInit:(CCCryptorRef *)cryptorRef operation:(CCOperation)op key:(NSData *)key iv:(NSData *)iv
{
        CCCryptorStatus status = CCCryptorCreate(
                                                 op, kCCAlgorithmAES128, 0x0000,
                                                 key.bytes, kCCKeySizeAES256,
                                                 iv.bytes, // initialisation vector
                                                 cryptorRef
                                                 );
        if (status == kCCSuccess) {
            //saveToLog(@"%@, %@, %@: %@, %@", @"I", @"AylaEncryption", @"start encrypt context", @"success", @"cipherInit");
            return true;
        }
        else{
            saveToLog(@"%@, %@, %@: %@, %@", @"E", @"AylaEncryption", @"start encrypt context", @"failed", @"cipherInit");
            return false;
        }
}


+ (NSData*)cipherEncrypt:(CCCryptorRef *)ref plainText:(NSString *)plainText
{
    plainText = [NSString stringWithFormat:@"%@\0",plainText];
    
    //Do padding
    long len = [plainText length];
    long pad = len % 16;
    pad = (pad>0)? 16-pad : pad;
    NSMutableData *padBuf = [NSMutableData dataWithLength:pad];
    NSMutableData *paddedData = [[NSMutableData alloc] initWithData:[plainText dataUsingEncoding:NSUTF8StringEncoding]];
    [paddedData appendData:padBuf];

    NSUInteger dataLength = [paddedData length];
    size_t dataSize = dataLength + kCCBlockSizeAES128;
    NSMutableData *cipherData = [NSMutableData dataWithLength:dataSize];
    
    size_t encryptedDataSize = 0;
    CCCryptorStatus cryptStatus = CCCryptorUpdate(*ref, paddedData.bytes, paddedData.length, cipherData.mutableBytes,  dataSize,  &encryptedDataSize);
    
    if (cryptStatus == kCCSuccess) {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        NSData *_cipherData = [cipherData subdataWithRange:NSMakeRange(0, encryptedDataSize)];
        return _cipherData;
    }
    return nil;
}

+ (NSString*)cipherDecrypt:(CCCryptorRef *)ref cipherData:(NSData *)cipherData
{    
    NSUInteger dataLength = [cipherData length];
    size_t dataSize = dataLength + kCCBlockSizeAES128;
    NSMutableData *clearData = [NSMutableData dataWithLength:dataSize];
    
    size_t decryptedDataSize = 0;
    CCCryptorStatus cryptStatus = CCCryptorUpdate(*ref, cipherData.bytes, cipherData.length, clearData.mutableBytes,  dataSize,  &decryptedDataSize);
    
    if (cryptStatus == kCCSuccess) {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        
        NSData *_clearData = [clearData subdataWithRange:NSMakeRange(0, decryptedDataSize)];
        NSString *str = [[NSString alloc] initWithData:_clearData encoding:NSUTF8StringEncoding];
        return [str stringByTrimmingCharactersInSet:
                [NSCharacterSet controlCharacterSet]];
    }
    return nil;
}

+ (NSData*)lanModeEncryptInStream:(NSString *)plainText
{
    return [AylaEncryption cipherEncrypt:&eCipher plainText:plainText];
}

+ (NSString*)lanModeDecryptInStream:(NSData *)cipherData
{
    return [AylaEncryption cipherDecrypt:&dCipher cipherData:cipherData ];
}

+ (void)clean
{
    [AylaEncryption cipherClean:&eCipher];
    [AylaEncryption cipherClean:&dCipher];
}

+ (void)cipherClean:(CCCryptorRef *)ref
{
    if (*ref != nil) {
        //size_t dataSize = kCCBlockSizeAES128;
        //size_t dataOutput;
        //NSMutableData *clearData = [NSMutableData dataWithLength:dataSize];
        //CCCryptorFinal(*ref, clearData.mutableBytes,
        //               kCCBlockSizeAES128, &dataOutput);
        CCCryptorRelease(*ref);
        *ref = nil;
    }
}


+ (NSData*)hmacForKeyAndData:(NSData*)key data:(NSData*)data
{
    if(key==nil)
        return [@"-" dataUsingEncoding:NSUTF8StringEncoding];
    
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    bzero(cHMAC, CC_SHA256_DIGEST_LENGTH);
    CCHmac(kCCHmacAlgSHA256, key.bytes, key.length, data.bytes, data.length, cHMAC);
    NSData *hmac = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    return hmac;
}


+ (void)cleanCurrentSession
{
    //clear current session
    keyId1 = [NSNumber numberWithInt:-1];
    appSignKey = nil;
    devSignKey = nil;
    [AylaEncryption clean];
}

//-----------TokenGeneration-----------------------------
+ (NSString*)randomToken:(int)len
{    
    static NSString *list = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    int list_len =62;
    NSMutableString *s = [NSMutableString stringWithCapacity:len];
    for (NSUInteger i = 0U; i < len; i++) {
        u_int32_t r = arc4random() % list_len;
        unichar c = [list characterAtIndex:r];
        [s appendFormat:@"%C", c];
    }
    return s;
}

+ (NSData *)base64Decode:(NSString *)string
{
    return [NSData dataFromBase64String:string];
}


+ (NSString*)dataToHexString:(NSData *)data
{
    NSData *ddata = data;
    NSUInteger capacity = [data length] * 2;
    NSMutableString *hexString = [NSMutableString stringWithCapacity:capacity];
    const unsigned char *dataBuffer = [data bytes];
    NSInteger i;
    for (i=0; i<[ddata length]; ++i) {
        [hexString appendFormat:@"%02lX", (unsigned long)dataBuffer[i]];
    }
    return hexString;
}


+ (NSData *)dataFromHexString:(NSString *)hexString
{
    NSMutableData *stringData = [[NSMutableData alloc] init];
    unsigned char whole_byte;
    char byte_chars[3] = {'\0','\0','\0'};
    int i;
    for (i=0; i < [hexString length] / 2; i++) {
        byte_chars[0] = [hexString characterAtIndex:i*2];
        byte_chars[1] = [hexString characterAtIndex:i*2+1];
        whole_byte = strtol(byte_chars, NULL, 16);
        [stringData appendBytes:&whole_byte length:1];
    }
    return stringData;
}



@end
