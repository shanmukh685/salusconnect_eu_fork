//
//  AylaError.m
//  Ayla Mobile Library
//
//  Created by Yipei Wang on 8/9/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import "AylaError.h"
#import "AylaResponseSupport.h"

@interface AylaError ()
@property (nonatomic, readwrite) id errorInfo;
@property (nonatomic, readwrite) id nativeErrorInfo;
@property (nonatomic, readwrite) NSInteger errorCode;
@end

@implementation AylaError

- (id)initWithAylaResponse:(AylaResponse *)reponse
{
    self = [super self];
    if(self && reponse) {
        self.httpStatusCode = reponse.httpStatusCode;
    }
    return self;
}

- (id)initWithNSDictionary:(NSDictionary *)dictionary
{
    self = [super initWithNSDictionary:dictionary];
    if(self) {
        self.errorInfo = [dictionary objectForKey:[NSNumber numberWithInt:AylaErrorParameterErrorInfo]]? nil: [dictionary objectForKey:[NSNumber numberWithInt:AylaErrorParameterErrorInfo]];
        
        self.nativeErrorInfo = [dictionary objectForKey:[NSNumber numberWithInt:AylaErrorParameterNativeErrorInfo]]? nil: [dictionary objectForKey:[NSNumber numberWithInt:AylaErrorParameterNativeErrorInfo]];
        
        self.errorCode = [dictionary objectForKey:[NSNumber numberWithInt:AylaErrorParameterErrorCode]]? 0: [(NSNumber *)[dictionary objectForKey:[NSNumber numberWithInt:AylaErrorParameterErrorCode]] integerValue];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [super copyWithZone:zone];
    if (copy) {
        AylaError *_copy = copy;
        _copy.errorInfo = self.errorInfo;
        _copy.nativeErrorInfo = self.nativeErrorInfo;
        _copy.errorCode = self.errorCode;
    }
    return copy;
}

+ (AylaError *)createWithCode:(NSInteger)errCode httpCode:(NSInteger)httpCode
                  nativeError:(NSError *)nativeError
               andErrorInfo:(NSDictionary *)dictionary
{
    AylaError *err = [AylaError new];
    err.httpStatusCode = httpCode;
    err.nativeErrorInfo = nativeError;
    err.errorInfo = dictionary;
    err.errorCode = dictionary? errCode: 1; //AML ERROR FAIL
    return err;
}

@end
