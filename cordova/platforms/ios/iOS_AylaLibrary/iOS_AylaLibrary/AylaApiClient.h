//
//  AylaApiClient.h
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 6/4/12.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import <AFNetworking/AFHTTPRequestOperationManager.h>

#define AML_SERVICE_LOCATION_CODE_US @""
#define AML_SERVICE_LOCATION_CODE_CN @"-cn"

@interface AylaApiClient : AFHTTPRequestOperationManager
@property (nonatomic, copy) NSString *deviceServicePath;             // base URL to Ayla Device Service
@property (nonatomic, copy) NSString *userServicePath;               // base URL to Ayla User Service
@property (nonatomic, copy) NSString *applicationTriggerServicePath; // base URL to Ayla ApplicationTrigger Service
@property (nonatomic, copy) NSString *connectedDevicePath;           // base URL to a device that has completed setup and is connecto to the Ayla device service
@property (nonatomic, copy) NSString *connectNewDevicePath;          //base URL to ayla new device
@property (nonatomic, copy) NSString *logServicePath;

- (NSOperation *)getPath:(NSString *)path
     parameters:(NSDictionary *)parameters
        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (NSOperation *)postPath:(NSString *)path
      parameters:(NSDictionary *)parameters
         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (NSOperation *)putPath:(NSString *)path
     parameters:(NSDictionary *)parameters
        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (NSOperation *)deletePath:(NSString *)path
        parameters:(NSDictionary *)parameters
           success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method
                                      path:(NSString *)path
                                parameters:(NSDictionary *)parameters;

- (void)enqueueHTTPRequestOperation:(AFHTTPRequestOperation *)operation;

+ (id)sharedDeviceServiceInstance;
+ (id)sharedUserServiceInstance;
+ (id)sharedAppTriggerServiceInstance;
+ (id)sharedConnectedDeviceInstance: (NSString *)url;
+ (id)sharedNonSecureDeviceServiceInstance;
+ (id)sharedNewDeviceInstance;
+ (id)sharedLogServiceInstance;
@end

