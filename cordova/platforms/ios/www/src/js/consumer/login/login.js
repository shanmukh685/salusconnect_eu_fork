"use strict";

define([
	"app",
	"application/Router",
	"consumer/login/login.controller"
], function (App, AppRouter, Controller) {

	App.module("Consumer.Login", function (Login, App) {

		Login.Router = AppRouter.extend({
			appRoutes: {
				"(login)": "login",
				"login/(:type)(?confirmation_token=:token)": "login",
				"pinpad": "pinpad"
			}
		});

		App.addInitializer(function () {
			Login.router = new Login.Router({
				controller: Controller
			});
		});

		/* Full url to the image */
		Login.backgroundImageUrl = null;
	});

	return App.Consumer.Login;

});