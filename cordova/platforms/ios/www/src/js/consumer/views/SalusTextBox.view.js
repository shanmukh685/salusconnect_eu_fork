"use strict";

define([
	"app",
	"jquery",
	"underscore",
	"backbone.marionette",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, $, _, Mn, consumerTemplates, SalusViewMixin) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {
		Views.SalusTextBox = Mn.ItemView.extend({
			template: consumerTemplates["SalusTextBox"], // jshint ignore:line
			className: "salus-icon-textbox-wrp",
			events: {
				"focus input": "handleFormFocus",
				"blur input": "handleFormBlur",
				"keyup input": "inputChanged"
			},
			ui: {
				"errorLabel": "div.bb-error-label",
				"input": "input",
				"labelWrapper": "span.bb-label-wrapper"
			},
			defaultRenderTemplate: function () {
				return {
					labelClass: "",
					iconPath: "",
					iconAlt: "",
					labelId: _.uniqueId("salusTextBox"),
					inputClass: "",
					inputPlaceholder: "",
					inputType: "input",
					isRequired: false,
					hideErrorTriangle: false,
					hideArrows: false
				};
			},
			initialize: function (renderTemplate) {
				this.renderTemplate = renderTemplate;
				this.$el.addClass(renderTemplate.wrapperClassName);
			},
			handleFormFocus: function () {
				this.ui.labelWrapper.addClass("focused-border-color");
			},
			handleFormBlur: function () {
				this.ui.labelWrapper.removeClass("focused-border-color");
			},
			onRender: function () {
				return this;
			},
			templateHelpers: function () {
				return _.extend(this.defaultRenderTemplate(), this.renderTemplate);
			},
			val: function () {
				return this.ui.input.val();
			},
			clearVal: function () {
				this.ui.input.val("");
			},
			inputChanged: function () {
				this.ui.input.addClass("text-changed");
			},
			registerError: function (key) {
				this.$el.toggleClass("hasError", true);
				this.ui.errorLabel.text(App.translate(key));
				this.ui.input.removeClass("text-changed");
			},
			clearError: function () {
				this.$el.toggleClass("hasError", false);
				this.ui.errorLabel.text("");
				this.ui.input.removeClass("text-changed");
			}

		}).mixin([SalusViewMixin]);

	});

	return App.Consumer.Views.SalusTextBox;
});