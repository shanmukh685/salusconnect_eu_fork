"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/consumer.views",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/Breadcrumbs.view",
	"consumer/views/SalusLinkButton.view",
	"consumer/views/SalusModal.view",
	"consumer/equipment/views/DeleteEquipmentModal.view",
	"consumer/equipment/views/EquipmentSettingsWidget.view",
	"consumer/settings/views/CategorySelectWidget.view",
	"consumer/settings/views/EquipmentSettingsPage.view",
	"consumer/equipment/views/widgets/EquipmentAliasEdit.view",
	"consumer/settings/models/SettingsContentViewModels",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/models/SalusDropdownViewModel",
	"consumer/views/mixins/mixin.equipmentPanel"
], function (App, constants, templates, ConsumerViews, SalusPageMixin, SalusView, Breadcrumbs,
			SalusLinkButtonView, SalusModalView, DeleteEquipModalView, EquipmentSettingsWidget,
			CategorySelectWidget) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {

		Views.CategorySettingsPageView = Mn.LayoutView.extend({
			id: "bb-equipment-settings-page",
			className: "category-settings-page  settings-page-widgets container",
			template: templates["settings/equipment/equipmentPage"],
			regions: {
				breadcrumbs: ".bb-breadcrumbs",
				removeEquipment: "#bb-remove-equipment",
				infoRegion: "#bb-info-region",
				settingsRegion: "#bb-settings-region",
				aliasRegion: ".bb-alias-edit",
				cancelButton: ".bb-button-cancel",
				saveButton: ".bb-button-save"
			},
			ui: {
				icon: ".bb-equipment-icon",
				pageHeader: ".bb-equipment-name"
			},
			events: {
				"click .bb-pencil-icon": "_editDeviceAlias"
			},
			initialize: function (options) {
				_.bindAll(this, "handleSubmitClicked", "handleCancelClick");

				this.type = options.type;

				var validCategory = _.contains(_.values(constants.categoryTypes), options.type);
				if (!validCategory) {
					App.navigate("");
				}
			},
			onRender: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise(["devices"]).then(function (/*arrayOfData*/) {
					// check if group exits
					// equipment.myEquipment.categories.types.<type>
					var categoryTypeNameKey = "equipment.myEquipment.categories.types." + that.type;
					var categoryTypeName = App.translate(categoryTypeNameKey);

					// get all of our devices grouped by category
					var allEquipmentAsCategories = App.salusConnector.getDeviceCollectionSortedByCategory();
					// filter by the selected category
					var categoryArray = allEquipmentAsCategories.filter(function (category) {
						//TODO: change this into used the passed in category
						return category.get("category_type") === that.type;
					});

					that.breadcrumbs.show(new Breadcrumbs.BreadcrumbsView(
						{ crumbs: [
							{
								textKey: "equipment.myEquipment.title",
								href: "/equipment"
							},
							{
								textKey: categoryTypeNameKey,
								href: "/equipment/categories/" + that.type
							},
							{
								textKey: "common.header.settings",
								active: true
							}
						]}
					));

					that.ui.pageHeader.text(categoryTypeName);
					that.$(".bb-spinner").remove(); //Removes spinner.

					//now filter the devices by model type
					if (categoryArray.length > 0) {
						var equipmentInCategory = categoryArray[0];
						var equipmentInCategoryArray = equipmentInCategory.get("category_devices");
						var equipmentByModelType = {};

						equipmentInCategoryArray.forEach(function (device) {
							var modelType = device.modelType;

							if (modelType in equipmentByModelType) {
								equipmentByModelType[modelType].push(device);
							} else {
								equipmentByModelType[modelType] = [device];
							}
						});

						that.equipmentByModelType = equipmentByModelType;
						that.sortedKeys = Object.keys(equipmentByModelType).sort();

						if (that.sortedKeys.length > 0) {
							// grab the first object out of the first device group.
							that.deviceModel = equipmentByModelType[that.sortedKeys[0]][0];
							that.currDeviceType = that.sortedKeys[0];
						}

						if (that.deviceModel.get("equipment_page_icon_url")) {
							that.ui.icon.css("background-image", "url(" + that.deviceModel.get("equipment_page_icon_url") + ")");
						}

						that.selectCategoryView = new CategorySelectWidget({
							sortedKeys: that.sortedKeys,
							equipmentByModelType: that.equipmentByModelType
						});

						that.infoRegion.show(that.selectCategoryView);

						// listenTo when the dropdown changes
						that.listenTo(that.selectCategoryView, "changed:selectedModel", that.updateSettingsView);

						var viewModelOption = {deviceModel: that.deviceModel};
						that.settingPanel = new Views.EquipmentSettingsPanel({
							header: {
								textKey: "equipment.settings.title"
							},
							cssClass: "settingsWidget",
							model: new  App.Consumer.Settings.Models.EquipmentSettingsPanelModel(viewModelOption)
						});
						that.settingsRegion.show(that.settingPanel);

						that.deviceModel.getDeviceProperties().then(function () {
							that.settingsCollection = App.Consumer.Settings.Models.SettingsContentFactory.createViewCollection(that.deviceModel);
							that.settingPanel.updateModelCollection(that.settingsCollection);

							if (that.settingsCollection.length > 0 ) {
								that.cancelButton.show(new App.Consumer.Views.SalusButtonPrimaryView({
									buttonTextKey: "common.labels.cancel",
									className: "btn btn-default min-width-btn pull-right",
									clickedDelegate: that.handleCancelClick
								}));

								that.saveButton.show(new App.Consumer.Views.SalusButtonPrimaryView({
									buttonTextKey: "common.labels.save",
									classes: "min-width-btn",
									clickedDelegate: that.handleSubmitClicked
								}));
							}
						});
					} else {
						var noEquipmentView = new App.Consumer.Settings.Views.EmptyListView({
							tagName: "h2",
							className: "",
							i18nTextKey: "equipment.myEquipment.categories.noEquipment"
						});
						that.infoRegion.show(noEquipmentView);
					}
				});
			},
			handleSubmitClicked: function () {
				var that = this;

				// show modal if we have multiple devices with the same 'modelType'
				if (this.equipmentByModelType[this.currDeviceType].length > 1) {
					App.modalRegion.show(new SalusModalView({
						contentView: new App.Consumer.Equipment.Views.ChangeAllDevicesModalView({
							devices: this.equipmentByModelType[this.currDeviceType],
							modelType: this.currDeviceType,
							okClickedDelegate: function () {
								var selectedModelGroup = that.equipmentByModelType[that.currDeviceType];
								that.settingPanel.saveSettings(selectedModelGroup);
								App.hideModal();
							}
						})
					}));

					App.showModal();
				} else {
					this.settingPanel.saveSettings();
				}
			},

			handleCancelClick: function () {
				this.settingPanel.resetSettings();
			},

			updateSettingsView: function (args) {
				var selectedVal = args.selectedVal;
				var selectedModelGroup = this.equipmentByModelType[this.sortedKeys[selectedVal]];
				this.currDeviceType = this.sortedKeys[selectedVal];

				if (selectedModelGroup.length > 0) {
					this.settingPanel.deviceModel = selectedModelGroup[0];
					this.settingPanel.render();
				}
			}
		}).mixin([SalusPageMixin], {
			analyticsSection: "settings",
			analyticsPage: "equipment" //TODO: This should be a function so the equipment Id is recorded.
		});
	});

	return App.Consumer.Settings.Views.CategorySettingsPageView;
});