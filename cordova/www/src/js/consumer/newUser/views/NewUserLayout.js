"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.registeredRegions",
	"consumer/setupWizard/mixins/mixin.setupWizardPage",
	"consumer/setupWizard/views/WelcomePageTitle.view",
	"consumer/newUser/views/NewUserForm"
], function (App, consumerTemplates, SalusPageMixin, RegisteredRegions, SetupWizardPage, WelcomePageTitle) {

	App.module("Consumer.NewUser.Views", function (Views, App, B, Mn) {
		Views.NewUserLayout = Mn.LayoutView.extend({
			template: consumerTemplates["newUser/newUserLayout"],
			regions: {
				"pageTitle": ".bb-page-title",
				"formRegion": ".bb-form-region"
			},
			initialize: function () {
				this.newUserModel = App.salusConnector.newUser();

				this.registerRegion("pageTitle", new WelcomePageTitle({}));
				this.registerRegion("formRegion", new Views.NewUserForm({
					model: this.newUserModel
				}));

			},
			onBeforeDestroy: function () {
				this.newUserModel.destroy();
			}
		}).mixin([SalusPageMixin, RegisteredRegions, SetupWizardPage], {
			analyticsSection: "newUser",
			analyticsPage: "newUser"
		});
	});

	return App.Consumer.NewUser.Views.NewUserLayout;
});