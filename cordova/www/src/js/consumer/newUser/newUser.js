"use strict";

define([
	"app",
	"application/Router",
	"consumer/newUser/newUser.controller"
], function (App, AppRouter, Controller) {

	App.module("Consumer.NewUser", function (NewUser, App) {
		NewUser.Router = AppRouter.extend({
			appRoutes: {
				"newUser": "newUser"
			}
		});

		App.addInitializer(function () {
			return new NewUser.Router({
				controller: Controller
			});
		});
	});

	return App.Consumer.NewUser;
});