"use strict";

define([
	"app",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/login/views/WelcomeWell.view",
	"consumer/login/views/ForgotPasswordWell.view",
	"consumer/login/views/SignInWell.view",
	"consumer/login/views/ChangePasswordWithTokenWell.view",
	"consumer/login/views/SignInWithTokenWell.view",
	"consumer/login/views/ConfirmAccountWell.view",
	"consumer/login/views/SendConfirmationEmailWell.view",
	"backstretch"
], function (App, config, consumerTemplates, SalusPageMixin) {

	App.module("Consumer.Login.Views", function (Views, App, B, Mn) {
		var contentWellTypeViewMap = {
			"welcome": Views.WelcomeWellView,
			"forgotPassword": Views.ForgotPasswordWell,
			"changePasswordWithToken": Views.ChangePasswordWithTokenWell,
			"signIn": Views.SignInWell,
			"signInWithToken": Views.SignInWithTokenWell,
			"confirm": Views.ConfirmAccountWell,
			"sendConfirmationEmail": Views.SendConfirmationEmail
		};

		Views.PageContainer = Mn.LayoutView.extend({
			id: "bb-login-page-container",
			className: "container login-page",
			template: consumerTemplates["login/PageContainer"],
			ui: {
				headline: "#bb-well-headline"
			},
			regions: {
				contentRegion: "#bb-well-content-region"
			},
			onRender: function () {
				var pageType = this.model.get("contentWellType"),
					contentWellView = new contentWellTypeViewMap[pageType]({
						"model": this.model
					});

				this.setAnalyticsPage(pageType);

				if (pageType === "confirm") {
					this.ui.headline.addClass("confirm");
					this.ui.headline.text(App.translate("login.confirm.headline"));
				} else {
					this.ui.headline.removeClass("confirm");
					this.ui.headline.text(App.translate("login.headline"));
				}

				this.contentRegion.show(contentWellView);

				if (!$("body").data("backstretch")) {
					var imageUrl = App.Consumer.Login.backgroundImageUrl,
						backgrounds = config.appBackgroundImages;

					if (!imageUrl) {
						var imagePth = backgrounds[Math.floor(Math.random() * backgrounds.length)];
						imageUrl = App.rootPath(imagePth);
						App.Consumer.Login.backgroundImageUrl = imageUrl;
					}

					$.backstretch(imageUrl);
				}
			},

			onDestroy: function () {
				//Adding true destroy prevents the image from going away. This will prevent a white background flash
				// between login page transitions.  But we still need a way to remove it once we transition to a none
				// login page and that is what the setTimeout is for.
				$.backstretch("destroy", true);
				setTimeout(function () {
					if (!$("body").data("backstretch")) {
						$("body > .backstretch").remove();
					}
				}, 0);
			}

		}).mixin([SalusPageMixin], {
			analyticsSection: "login"
		});
	});

	return App.Consumer.Login.Views.PageContainer;
});
