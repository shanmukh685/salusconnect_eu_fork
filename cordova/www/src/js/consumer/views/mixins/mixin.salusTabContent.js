"use strict";

define([
	"app",
	"consumer/views/mixins/mixin.salusView"
], function (App, SalusView) {

	App.module("Consumer.Views.Mixins", function (Mixins) {
		Mixins.SalusTabContentMixin = function (options) {
			this.mixin([
				SalusView // by mixing in SalusTextBox we also mixin SalusView
			], options);

			this.after('initialize', function () {
				this.$el.addClass("bb-tab-content tab-pane");
			});
		};
	});

	return App.Consumer.Views.Mixins.SalusTabContentMixin;
});