"use strict";

define([
	"app",
	"common/constants",
	"common/AylaConfig",
	"common/config",
	"common/model/ayla/deviceTypes/mixin.BaseDevice"
], function (App, constants, AylaConfig, config, BaseAylaDevice) {

	App.module("Models", function (Models, App, B) {
		/**
		 * this is a wrapper for testing ayla base device. it allows us to construct one and run unit tests and such
		 */
		Models.CoordinatorDevice = B.Model.extend({
			tileType: "wud",
			modelType: constants.modelTypes.COORDINATORDEVICE,
			defaults: {
				// get properties
				FirmwareVersion: null, //string
				MACAddress: null, //string length 16
				SubDeviceName_c: null, //string - not used _c naming is as defined on device wiki
				DeviceType: null, //integer  2:iT600 TRV
				ManufactureName: null, //string length 32
				ModelIdentifier: null, //string length 32
				DeviceIndex: null, //integer  10: Receiver 1, 11: Receiver 2
				SetIndicator: null, //integer Set Device Identification Indicator with duration
				SetPermitJoinPeriod: null //integer Set permit join period 0-254 minutes, 255 means forever
			},
			initialize: function (/*data, options*/) {
				//TODO Setup
				this._setCategoryDefaults();
				this._setEquipmentPageDefaults();
			},
			_setCategoryDefaults: function () {
				this.set({
					device_category_id: 2,
					device_category_icon_url: App.rootPath("/images/icons/dashboard/icon_wud.svg"),
					device_category_type: constants.categoryTypes.WIRELESSUNCONTROLLABLEDEVICES,
					device_category_name_key: "equipment.myEquipment.categories.types.wirelessUncontrollableDevices"
				});
			},
			_setEquipmentPageDefaults: function () {
				this.set({
					equipment_page_icon_url: App.rootPath("/images/icons/myEquipment/icon_wud_grey.svg")
				});
			},
			initiatePairingMode: function () {
				return this.get("SetPermitJoinPeriod").setProperty(config.setPermitJoinPeriodLength);
			},
			deactivatePairingMode: function () {
				return this.get("SetPermitJoinPeriod").setProperty(0);
			}
		}).mixin([BaseAylaDevice]);
	});

	return App.Models.CoordinatorDevice;
});
