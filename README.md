This is the Salus connected solution frontend repository.

# Instructions for Local Development
Install node.js and npm
Install vagrant;

execute the following commands from the root of the project 

vagrant up
vagrant ssh
npm start

this will start the watcher which compiles all code, start the nginx instance on the vagrant box, and download all npm dependencies

The local site should now be available at localhost:8080/


# Deployment

## Build Web Instructions
Install Node
Install npm

Execute the following command from the root of the project

## deploy to http://210.75.16.197:8080
export BUILD=034
export VERSIONNUMBER=1.0.6
export RELEASEDATE=
export ENVIRONMENT=prod
./tools/buildForDeployment.py
然后将output目录下的文件ftp到210下的/var/www/html/salusconnect

## deploy to eu us www.salusconnect.io
clone salus发布git:
git clone https://bitbucket.org/michael_chaney/salus-web.git

export BUILD=025
export VERSIONNUMBER=1.0.2
export RELEASEDATE=
export ENVIRONMENT=prod
./tools/buildForDeployment.py
然后将output目录下的文件
cp -R web/output/. /var/www/salus-web/html/
将修改提交
ssh 登录到210服务器,然后在ssh分别登录下面的服务器
cd ~/.ssh/
ssh -i ./id_rsa kairy@www.salusconnect.io 
ssh -i ./id_rsa kairy@eu.salusconnect.io 
ssh -i ./id_rsa kairy@us.salusconnect.io 
登录后
cd /var/www/html
git pull

This process will install any npm dependencies and build up the static assets for deployment.

Once the process completes the static asset ouputs will reside in web/output. 


##These files can be scp to the server using the following command.
##暂时不用
##./tools/deployAssets.py brian@52.25.165.98 /var/www/html insert_path_to_pem_key


## Build Android Instructions
Install gradle 2.2

修改cordova/config.xml的版本号

export BUILD=android-269
export VERSIONNUMBER=0.2.3
export RELEASEDATE=
export ENVIRONMENT="prod"

./tools/buildForDeployment.py

cd cordova

./../node_modules/cordova/bin/cordova prepare android -d

cd platforms/android

gradle assembleRelease


## Build iOS Instructions


Setup environment variables: Leave RELEASEDATE empty if you want the current date.

修改cordova/config.xml的版本号

export BUILD=ios-269
export VERSIONNUMBER=0.2.3
export RELEASEDATE=
export ENVIRONMENT="prod"
./tools/buildForDeployment.py

open xcode ./cordova/platforms/ios/Connected-Solution.xcworkspace

Update Bundle Identifier for production:
Update version and build numbers.
Verify/Update Team.

Build for Archive.
In Organizer window click latest archive and then click upload


# Development

##
To recreate cordova  (for ios atleast)
verify: sudo npm install -g cordova
run: cordova create cordova com.computime.salus.connected Connected-Solution
run: cd cordova
run: cordova platforms add ios
run: mv cordova/platforms/ios/ .


## Email Templates
TODO:

## Translation Files
There are two primary translation files for development. The Dev (web/src/translations/dev/consumer.json) file and the
English (web/src/translations/en/consumer.json) files.

The English file contains the production ready text for display to the user.
  Exmaple {
          	"common": {
          		"brand": "Salus",
          		"countries": {
          			"at": "Austria",
          			"be": "Belgium",
          		},
          		"daysOfWeek": {
          			"abbreviations": {
          				"friday": "Fr"
          			}
          		}
          	}
          }

The Dev file has the long key name used within the source code. This help during development for finding missing or inaccurate keys.
 Example {
         	"common": {
         		"brand": "common.brand",
         		"countries": {
         			"at": "common.countries.at",
         			"be": "common.countries.be",
         		},
         		"daysOfWeek": {
         			"abbreviations": {
         				"friday": "common.daysOfWeek.abbreviations.friday"
         			}
         		}
         	}
         }

To generate files run: gulp importTranslationFiles