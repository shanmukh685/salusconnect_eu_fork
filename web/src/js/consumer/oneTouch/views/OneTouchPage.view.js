"use strict";

define([
	"app",
	"common/config",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusView",
	"common/util/VendorfyCssForJs",
	"consumer/oneTouch/views/RecommendedRulesLayout.view",
	"consumer/equipment/views/myEquipment/AddNewTile.view",
	"common/model/salusWebServices/rules/Rule.model"
], function (App, config, constants, consumerTemplates, SalusPage, SalusView, VendorfyCss) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {
		Views.OneTouchPageView = Mn.LayoutView.extend({
			className: "container",
			id: "one-touch-page",
			template: consumerTemplates["oneTouch/oneTouchPage"],
			ui: {
				noGatewayText: ".bb-no-gateway-text"
			},
			regions: {
				automationsRegion: ".bb-automations-region",
				recommendedRegion: ".bb-recommended-region"
			},
			initialize: function () {
				_.bindAll(this,
					"initiateRefreshPoll",
					"clearRefreshPollTimeout",
					"refreshPoll"
				);

				this.pollingInterval = null;
			},
			onBeforeDestroy: function() {
				this.clearRefreshPollTimeout();
			},
			onRender: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise(["devices"]).then(function (/*arrayOfData*/) {
					var ruleCollection = App.salusConnector.getRuleCollection();

					if (ruleCollection) {
						ruleCollection.refresh();
					}

					if (!App.hasCurrentGateway()) {
						that.ui.noGatewayText.removeClass("hidden");
					}

					that.automationsRegion.show(new Views.OneTouchCollectionView({
						collection: App.salusConnector.getDisplayRuleCollection()
					}));

					if (App.salusConnector.shouldListRecommendedRules()) {
						that.recommendedRegion.show(new Views.RecommendedRulesLayoutView());
					}
				});
				
				this.initiateRefreshPoll();
			},
			initiateRefreshPoll: function () {
				this.pollingInterval = setInterval(this.refreshPoll, config.devicePollingInformation.oneTouchsPollingInterval);
			},
			clearRefreshPollTimeout: function () {
				clearInterval(this.pollingInterval);
			},
			refreshPoll: function () {
				var ruleCollection = App.salusConnector.getRuleCollection();

				if (ruleCollection) {
					ruleCollection.refresh();
				}
			}
		}).mixin([SalusPage], {
			analyticsSection: "oneTouch",
			analyticsPage: "list"
		});

		Views.OneTouchTileView = Mn.ItemView.extend({
			template: consumerTemplates["oneTouch/oneTouchTile"],
			className: "one-touch-tile col-xs-6 col-sm-4",
			attributes: {
				role: "button"
			},
			events: {
				"click": "_handleClick"
			},
			ui: {
				tileBackground: ".bb-one-touch-tile-background"
			},
			initialize: function () {
				_.bindAll(this, "_handleClick", "_navigateDetailPage");
			},
			onRender: function () {
				VendorfyCss.formatBgGradientWithImage(
					this.ui.tileBackground,
					"center 52% / 40%",
					"no-repeat",
					App.rootPath("/images/icons/dashboard/icon_one_touch.svg"),
					"145deg",
					constants.tileGradients.TILE_GREEN_GRADIENT_TOP,
					"0%",
					constants.tileGradients.TILE_GREEN_GRADIENT_BOTTOM,
					"100%"
				);
			},
			_handleClick: function () {
				var uriKey = encodeURIComponent(this.model.get("key"));

				this._navigateDetailPage(uriKey);
			},
			_navigateDetailPage: function (OTKey) {
				App.navigate("oneTouch/" + App.getCurrentGatewayDSN() + "/" + OTKey);
			}
		}).mixin([SalusView]);

		Views.OneTouchCollectionView = Mn.CollectionView.extend({
			className: "row one-touch-collection",
			template: false,
			childView: Views.OneTouchTileView,
			initialize: function () {
				_.bindAll(this, "updateCollection");

				this.listenTo(App.salusConnector.getRuleCollection(), "update", this.updateCollection);
			},
			onRender: function () {
				if (!this.addNewAutomationTile) {
					var key = "equipment.oneTouch.automations.addNew",
						clickDestination = "oneTouch/" + App.getCurrentGatewayDSN() + "/create";

					if (!App.getCurrentGateway()) {
						key = "myStatus.addNewGateway";
						clickDestination = "setup/gateway";
					}

					this.addNewAutomationTile = new App.Consumer.Equipment.Views.AddNewTileView({
						i18nTextKey: key,
						size: "regular",
						classes: "one-touch-tile",
						clickDestination: clickDestination
					});
				}

				this.$el.prepend(this.addNewAutomationTile.render().$el);
			},
			onDestroy: function () {
				if (this.addNewAutomationTile) {
					this.addNewAutomationTile.destroy();
				}
			},
			updateCollection: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise("rules").then(function () {
					that.collection.set(App.salusConnector.getDisplayRuleCollection().toArray());
				});
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.OneTouch.Views.OneTouchPageView;
});