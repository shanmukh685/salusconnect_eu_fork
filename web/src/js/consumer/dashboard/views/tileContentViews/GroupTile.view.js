"use strict";

define([
	"app",
	"common/config",
	"common/constants",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"common/util/VendorfyCssForJs",
	"common/util/utilities"
], function (App, config, constants, P, consumerTemplates, SalusView, VendorfyCssForJs, utilities) {
  App.module("Consumer.Dashboard.Views.TileContentViews", function (Views, App, B, Mn, $, _) {
    Views.GroupTile = {};
    Views.GroupTile.FrontView = Mn.ItemView.extend({
      template: consumerTemplates["equipment/myEquipment/groupsTile"],
      className: "group-tile",
      attributes: {
        "role": "button"
      },
      ui: {
        alert: ".bb-group-alert-icon",
        pin: ".bb-pin-to-dashboard",
        gradientWithIcon: ".bb-group-tile-background"
      },
      events: {
        click: "_handleClick"
      },
      onRender: function () {
        this._setIdleStyleOnTile();
      },
      _setIdleStyleOnTile: function () {
        VendorfyCssForJs.formatBgGradientWithImage(
            this.ui.gradientWithIcon,
            "center 50%/48% 48%",
            "no-repeat",
            App.rootPath("/images/icons/dashboard/icon_groups.svg"),
            "145deg",
            constants.tileGradients.TILE_GREEN_GRADIENT_TOP,
            "0%",
            constants.tileGradients.TILE_GREEN_GRADIENT_BOTTOM,
            "100%"
        );
      },
      /**
       * TODO Wire this up to actual alerts when those occur
       * @private
       */
      _setAlertStyleOnTile: function () {
        VendorfyCssForJs.formatBgGradientWithImage(
            this.ui.gradientWithIcon,
            "center 50%/48% 48%",
            "no-repeat",
            App.rootPath("/images/icons/dashboard/icon_groups.svg"),
            "145deg",
            constants.tileGradients.TILE_RED_GRADIENT_TOP,
            "0%",
            constants.tileGradients.TILE_RED_GRADIENT_BOTTOM,
            "100%"
        );
      },
      _handleClick: function (/*evt*/) {
        // todo: check click was on pin, pin group to dashboard
        // this._goToGroupPage(this.model);
      },
      _goToGroupPage: function (group) {
        App.navigate("equipment/groups/" + group.get("key"));
      }
    }).mixin([SalusView]);
  });
});
