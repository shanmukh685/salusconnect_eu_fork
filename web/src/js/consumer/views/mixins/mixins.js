"use strict";

define([
	"app",
	"consumer/views/mixins/mixin.registeredRegions",
	"consumer/views/mixins/mixin.salusButton",
	"consumer/views/mixins/mixin.salusCollapsableView",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusTabContent",
	"consumer/views/mixins/mixin.salusTextBox",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.validation",
	"consumer/views/mixins/mixin.formWithValidation",
	"consumer/views/mixins/mixin.thermostatArcView",
	"consumer/views/mixins/mixin.batteryLevel"
], function (App) {

	return App.Consumer.Views.Mixins;
});