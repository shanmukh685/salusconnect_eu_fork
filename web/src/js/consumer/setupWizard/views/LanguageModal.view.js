"use strict";

define([
	"app",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/modal/ModalCloseButton.view",
	"consumer/views/SalusDropdown.view",
	"consumer/models/SalusDropdownViewModel"
], function (App, config, consumerTemplates, SalusView, SalusPrimaryButtonView) {
	App.module("Consumer.SetupWizard.Views", function (Views, App, B, Mn, $, _) {
		Views.LanguageModalView = Mn.LayoutView.extend({
			className: "modal-body language-modal padding-t-40 padding-b-40",
			template: consumerTemplates["setupWizard/languageModal"],
			regions: {
				dropdownRegion: ".bb-language-dropdown",
				saveButtonRegion: ".bb-save-button"
			},
			modelEvents: {
				"change:language": "enableSave"
			},
			initialize: function () {
				_.bindAll(this, "enableSave", "handleSaveClick");

				var languageCollection = new App.Consumer.Models.DropdownItemViewCollection();
				this._fillDropdownCollection(config.languages, config.languagePrefix, languageCollection);

				this.languageDropdownView = new App.Consumer.Views.SalusDropdownView({
					collection: languageCollection,
					viewModel: new App.Consumer.Models.DropdownViewModel({
						id: "languageSelect",
						dropdownLabelKey: "settings.profile.formFields.languageSelect"
					}),
					defaultSelectionValue: this.model.get("language"),
					boundAttribute: "language",
					model: this.model
				});

				this.saveButtonView = new SalusPrimaryButtonView({
					id: "save-button",
					classes: "width100 disabled",
					buttonTextKey: "common.labels.save",
					clickedDelegate: this.handleSaveClick
				});
			},
			onRender: function () {
				this.dropdownRegion.show(this.languageDropdownView);
				this.saveButtonRegion.show(this.saveButtonView);
			},
			_fillDropdownCollection: function (list, prefix, collection) {
				_.each(list, function (key) {
					collection.add(new App.Consumer.Models.DropdownItemViewModel({
						value: key,
						displayText: prefix + "." + key
					}));
				});
			},
			enableSave: function () {
				if (this.model.hasLanguage()) {
					this.saveButtonView.enable();
				} else {
					this.saveButtonView.disable();
				}
			},
			handleSaveClick: function () {
				var that = this;

				this.saveButtonView.showSpinner();

				this.model.persist().then(function () {
					var language = that.model.get("language");
					App.changeLanguage(language).catch(function (err) {
						//ingnore lanugage change error. will default to dev/english
						App.error("Language file " + language + " : " + err);
					}).then(function () {
						that.saveButtonView.hideSpinner();
						App.hideModal();
						App.refreshPage();
					});
				});
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.SetupWizard.Views.LanguageModalView;
});