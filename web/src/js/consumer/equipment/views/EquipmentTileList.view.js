"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/equipment/views/CreateSchedule.view",
	"consumer/equipment/views/myEquipment/AddNewTile.view",
	"consumer/equipment/views/myEquipment/GroupsTiles.view"
], function (App, constants, templates, SalusViewMixin) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn) {
		Views.EquipmentTileListView = Mn.CompositeView.extend({
			className: "group-category-device-list",
			template: templates["equipment/myEquipment/equipmentTileListComposite"],
			childViewContainer: ".bb-devices-list",
			childView: Views.GroupedDeviceView,
			ui: {
				scheduleMenu: ".bb-schedule-menu"
			},
			initialize: function (options) {
				options = options || {};

				this.groupOrCategory = options.type;
				this.categoryType = options.categoryType;

				if (this.groupOrCategory === constants.equipmentLists.category) {
					var shouldShowSchedules = this.categoryType === constants.categoryTypes.THERMOSTATS ||
						this.categoryType === constants.categoryTypes.WATERHEATERS ||
					 	this.categoryType === constants.categoryTypes.SMARTPLUGS;

					if (shouldShowSchedules) {
						this.createSchedulesView = new Views.CreateScheduleView({
							categoryType: this.categoryType
						});
					}
				}
			},
			onRender: function () {
				// don't display a button for adding equipment if we came from the groups page
				if (this.groupOrCategory === constants.equipmentLists.category) {

					// show schedules view conditionally
					if (this.createSchedulesView) {
						this.ui.scheduleMenu.append(this.createSchedulesView.render().$el);
					} else {
						this.ui.scheduleMenu.detach();
					}

					if (!this.addEquipmentTile) {
						this.addEquipmentTile = new App.Consumer.Equipment.Views.AddNewTileView({
							i18nTextKey: "equipment.myEquipment.groups.addNewEquipment",
							size: "regular",
							classes: "grouped-tile",
							clickDestination: "setup/pairing"
						});
					}

					this.$(".bb-devices-list").prepend(this.addEquipmentTile.render().$el);
				} else if (this.groupOrCategory === constants.equipmentLists.group) {
					this.ui.scheduleMenu.detach();

					if (this.collection.isEmpty()) {
						// display an edit button for empty groups
						if (!this.groupEquipmentTile) {
							this.groupEquipmentTile = new App.Consumer.Equipment.Views.AddNewTileView({
								i18nTextKey: "equipment.myEquipment.groups.groupEquipment",
								size: "regular",
								classes: "grouped-tile",
								clickDestination: "equipment/editGroup/" + this.options.groupId
							});
						}

						this.$(".bb-devices-list").prepend(this.groupEquipmentTile.render().$el);
					}
				}
			},
			onDestroy: function () {
				if (this.createSchedulesView) {
					this.createSchedulesView.destroy();
				}

				if (this.groupEquipmentTile) {
					this.groupEquipmentTile.destroy();
				}

				if (this.addEquipmentTile) {
					this.addEquipmentTile.destroy();
				}
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Equipment.Views.EquipmentTileListView;
});
